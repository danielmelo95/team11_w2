<!DOCTYPE html>
<html lang="en">
<head>
    <?php
    //DEBUG
		// error_reporting(-1);
		// ini_set('display_errors', 'On');
		// set_error_handler("var_dump");
	?>
	<title>WEBTE2 | Finálne zadanie</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
<!--===============================================================================================-->
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <meta name="google-signin-scope" content="profile email">
    <meta name="google-signin-client_id" content="826090786652-7uu82dad1i2hs3olt99djahd65nk1ikg.apps.googleusercontent.com">
    <!--===============================================================================================-->


<!--===============================================================================================-->
</head>
<body>
	<?php
		session_start();
		// print_r($_POST);
		// echo '<hr/>';
		// echo $_POST;
		// session_destroy();
	// var_dump( $_SESSION);
		unset($_SESSION['type']);
		unset($_SESSION['id']);
		unset( $_SESSION );
			// var_dump($_SESSION);

	?>
<!-- GOOGLE MAPS -->
	<?php 	
 
		include('php/googlesql.php');
		if(!isset($_POST) || empty($_POST) || $_POST['mapType']==0)
		{
			$addresses=returnAdresses(1);
			$number = returnCounts();
			echo '<script>
			var source='.$addresses.';
			var number='.$number.';
			</script>';
		
		}
		else{
			$addresses=returnAdresses(2);
			echo '<script>
			var source='.$addresses.';
			</script>';
		}
		?>

		<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js">
		    </script>
	    <script src="js/googleMaps.js"></script>
		    	<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBJQv-yG16VjTsUX6w1By2gjN8a0R0a5Nc&callback=startMap&libraries=places,geometry"
		  type="text/javascript">
	  	</script>
<!-- END OF GOOGLE MAPS -->

	<div class="limiter">
		<div class="container-login100" style="background-image: url('images/bg-01.jpg');text-align: center;">
			<div class="wrap-login100 p-l-55 p-r-55 p-t-65 p-b-54" style="width: 70%; text-align: center; margin-top: 5%">
					<span class="login100-form-title p-b-49">
                        Finálne zadanie z predmetu WEBTE 2
					</span>
					<br>
						<div class="container-login100-form-btn">
							<div class="wrap-login100-form-btn w-50">
                        	<div class="login100-form-bgbtn"></div>
								<a href="php/adminLogin.php" class="login100-form-btn" style="color:white;">
								Login/Sign Up
							</a>
                            </div>
                        	</div>
                            <br>

                            <div class="container-login100-form-btn">
							<div class="wrap-login100-form-btn w-50">
                        	<div class="login100-form-bgbtn"></div>
								<a href="php/techdoc.php" class="login100-form-btn" style="color:white;">
								Technical documentation
							</a>
                            </div>
                        	</div>
                        	<br>
                    <div id="map" style="width:500px;	height:500px;    margin: auto;"></div>
                    <br>
                    <div id="mapdata" style="width:500px;	height:500px;    margin: auto;">
                    	<?php
	                   		
                            echo 'Choose if you want to show school locations or adress locations
                            	<br><form action="index.php" method="post" >';
                           	echo '<select name="mapType" id="mapType" class=" form-control w-100">';
    		                    echo '<option value="0">Schools</option>';
            	                echo '<option value="1">Addresses</option>';
                        	echo '</select>';
                            // echo '<input type="submit" name="submit" value=Submit />';
                            echo '<br><div class="container-login100-form-btn">
                    		<div class="wrap-login100-form-btn w-50">
                        	<div class="login100-form-bgbtn"></div>
                        	<button type="submit" class="login100-form-btn" name="submit" value="Submit">
                        	    Submit
                        	</button>
                    		</div>
                			</div>';
                            echo '</form>';
                           
                            echo '<div id="loadbar">';
                       		if( $_POST['mapType']==1 ){
                       			$sqlQuery = "SELECT COUNT(*) as cntr FROM `User`";	
                       			echo "<br>Showing user locations";
                       		}
                   			else{
                   				$sqlQuery = "SELECT COUNT(*) as cntr FROM `School`";	
                   				echo "<br>Showing school locations";
                   			}
                   			require('config.php');
               				$connection = mysqli_connect($servername,$username,$password,$dbname);
                            $connection->set_charset("UTF8");
                            $result = mysqli_query($connection, $sqlQuery);
                            $sqlCount = $result->fetch_assoc(); 
                            echo '<table>';
               				echo '<tr><th>Loading:</th><th id="loaded">   </th><th> of </th><th> '.$sqlCount['cntr'].'  </th></tr>'; 
               				echo '</table>';
                       		echo '</div>';
                        ?>
                	</div>
                    <br>
                    <br>
                    <br>
					<div class="txt1 text-center p-t-54 p-b-20">
						<span>

	                       <!-- 	<div class="flex-c-m">
	                            <a href="php/perfTable.php" class="">
								Performance Table
								</a>
	                       	</div>
							<div class="flex-c-m">
		                    	<a href="php/loginEmail.php" class="">
								Authentication E-mail
								</a>
		                   	</div>
							<div class="flex-c-m">
		                    	<a href="php/registracia.php" class="">
								Registration - DB
								</a>
		                   	</div> -->
						</span>
					</div>
        </div>
		</div>
	</div>
	


<!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

</body>
</html>