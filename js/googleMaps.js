
var myLatLng = {lat: 48.905734, lng: 19.750576};
var directionsService ;
var directionsDisplay ;

var coordinates;
var geocoder;
var map;
var markers = [];

var geocoderSTATUS = 0;
var globalCounter = 0;
var markersSET = 0;

var markerCluster;

function startMap() {
  map = new google.maps.Map(document.getElementById("map"), {
  zoom: 6,
  center: myLatLng
  });

  geocoder = new google.maps.Geocoder();
  for(var i = 0; i< source.length; i++)
  {
      if(i == 0)
        insertMarkersFromDB(i);
      else{
        setTimeout(insertMarkersFromDB, 10000, i);
        if(i==source.length)
        {
          alert('Vsetky markery boli uspesne nacitane!');
        }
      }

  }
  setMapOnAll();
  //document.getElementById("loadbar").innerHTML=" ";
}



function insertMarkersFromDB(addressNumber){
      coordinates=source;
      var condition = 0;

      if (typeof number === 'undefined'){
        counts=1;
        condition = 1;
      }
      else
        counts=number;


      geocoder.geocode( { 'address': coordinates[addressNumber]}, function(results, status) {
           if (status == google.maps.GeocoderStatus.OK) 
           {
             //console.log(addressNumber);
             if(addressNumber != source.length - 1){
              if (addressNumber%2 == 0)
                document.getElementById("loaded").innerHTML=addressNumber+1;
            }
            else
              document.getElementById("loadbar").innerHTML="";
             geocoderSTATUS = 1;
             if(condition){
               addMarker(results[0].geometry.location, 0);
              }
              else
                addMarker(results[0].geometry.location, counts[addressNumber]);
           }else{
              setTimeout(insertMarkersFromDB, 2000, addressNumber);
           }
       });
    return ;
}

function addMarker(location, count) {
    if (count==0){
      markers.push( new google.maps.Marker({
      position: location,
      map: map
    })
  );
      return ;
    }else{
    markers.push( new google.maps.Marker({
        label:{
          color: "white",
          fontWeight: "bold",
          text: count
        },
        position: location,
        map: map,
        title: 'Pocet studentov: ' + count
      })
    );
  }
}

function setMapOnAll(map) {
  for (var i = 0; i < markers.length; i++) {
    markers[i].setMap(map);
  }
}

// Removes the markers from the map, but keeps them in the array.
function clearMarkers() {
  setMapOnAll(null);
}

// Shows any markers currently in the array.
function showMarkers() {
  setMapOnAll(map);
}

// Deletes all markers in the array by removing references to them.
function deleteMarkers() {
  clearMarkers();
  markers = [];
}

//=======================================================
//  TRASY
//=======================================================
var directionsDisplay = [];
var directionsService = [];

var startLoc={lat:48.152074,lng:17.073362};
var map;
//var $scope = {};

function startRouteMap() {
  map = new google.maps.Map(document.getElementById("map"), {
  zoom: 6,
  center: startLoc
  });

  for (var i =0; i<startLats.length;i++){
    directionsService[i] = new google.maps.DirectionsService;
    directionsDisplay[i] = new google.maps.DirectionsRenderer;
    var mapOptions = {
      zoom: 14,
      center: startLoc
    }
    calcRoute(startLats[i],startLngs[i],endLats[i],endLngs[i], i);
  }
  //setTimeout(calcRoute, 2000, startLats[i],startLngs[i],endLats[i],endLngs[i]);
}

function calcRoute(startlat,startlon,finishlat,finishlon, i) {
  var start = new google.maps.LatLng(startlat, startlon);
  var finish = new google.maps.LatLng(finishlat, finishlon);

  //console.log(startlat+','+startlon);

  var request = {
      origin: start,
      destination: finish,
      travelMode: google.maps.TravelMode["WALKING"]
  };
  directionsService[i].route(request, function(response, status) {
    if (status == 'OK') {
    //  console.log(' STATUS OK');
      directionsDisplay[i].setDirections(response);
    //  console.log(response);
    }
    else
    { 
      console.log('FAILED: ' + status );
      //setTimeout(calcRoute, 7000, startlat,startlon,finishlat,finishlon);
    }
  });
  directionsDisplay[i].setMap(map);
}
