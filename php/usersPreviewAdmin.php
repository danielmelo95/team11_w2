<!DOCTYPE html>
<html lang="en">
<head>
    <title>WEBTE2 | Zadanie 3</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="../images/icons/favicon.ico"/>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../fonts/iconic/css/material-design-iconic-font.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/animsition/css/animsition.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/daterangepicker/daterangepicker.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../css/util.css">
    <link rel="stylesheet" type="text/css" href="../css/main.css">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <!--===============================================================================================-->
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <meta name="google-signin-scope" content="profile email">
    <meta name="google-signin-client_id" content="826090786652-7uu82dad1i2hs3olt99djahd65nk1ikg.apps.googleusercontent.com">
    <!--===============================================================================================-->
    <script src="../js/sorttable.js"></script>
    <script src='../js/pdfmake.min.js'></script>
    <script src='../js/vfs_fonts.js'></script>
    <!--===============================================================================================-->


</head>
<body>
<!-- ======================================NAVBAR============================================================== -->

<?php
    session_start();
   if(!isset($_SESSION) || $_SESSION['type']!=4){
        header('Location: noPermission.php');

   }
    require ("navBar.php");
    echo generateNavbarAdmin();
   ?>
<!-- ======================================NAVBAR============================================================== --> 

<style>
    .csv-btn:hover{
        background-color: #d7ffff;
        transition: 1s;
    }
</style>


<div class="limiter">
    <div class="container-login100" style="background-image: url('../images/bg-01.jpg');">
        <div class="wrap-login100 p-l-55 p-r-55 p-t-65 p-b-54 w-100" style="margin-top: 4%">

<?php

require '../config.php';

$mysqli = new mysqli($hostname,$username,$password,$dbname);
$mysqli->set_charset("cp1250");
if (mysqli_connect_errno()) {
    echo "Failed to connect to MySQL: " . mysqli_connect_error();
}
else
{
    $sql = "SELECT `User`.id,first_name,last_name,email,`User`.`address` u_addres,name, `School`.address as s_address FROM `User` JOIN  `School`ON `User`.`school` = `School`.`id`";

    $result = $mysqli->query($sql);
    echo "<div>";
    if ($result->num_rows > 0) {
        //echo "<table class=\"sortable\"";
        echo "<table class = \"sortable\" id=\"table\" >";
        echo "
            <tr>
            <th>Id</th>
            
            <th>First Name</th>
            
            <th>Last Name</th> 
            
            <th>Email</th>
            
            <th>User Address</th>
            
            <th>School Name</th>
            
            <th>School Address</th>

            <th>View</th>
            
            </tr>
            ";
        while ($row = $result->fetch_object())
        {
            echo "<tr class=\" value\">";

            foreach ($row as $value)
            {
                echo "<td>" . iconv( "Windows-1250", "UTF-8", ($value)) . "</td>";
            }
            echo "<th><a class = 'fa fa-arrow-right' href='userViewAdmin.php?id=".$row->id."'></a></th>";
            echo "</tr>";

        }
        echo "</table>";

    }

    echo "</div>";
    // echo "<button onclick='save_pdf();'>Save</button>";
    echo'  <br><br>  <div class="wrap-login100-form-btn w-50">
        <div class="login100-form-bgbtn"></div>
            <button onclick="save_pdf();" class="login100-form-btn">Save as pdf</button>
        </div>
        </div>';
}

mysqli_close($mysqli);
?>



<script>
    function save_pdf() {
        var dd = {
            content: []
        };
        var table =     {
            style: 'tableExample',
            table: {
                widths: ['14%','14%','14%','14%','14%','14%','14%'],
                body: [
                ]
            }
        };
        table.table.body.push( ['Id', 'First Name', 'Last Name', 'Email','User Address', 'School Name', 'School Address']); //hlavicka push
        // console.log( ['Id', 'First Name', 'Last Name', 'Email', 'School Name', 'School Address']); //hlavicka push


        var data = document.getElementsByClassName("value");
        // console.log(data);
        var i,j;
        var len = data.length;


        for (i = 0; i < len; i++)
        {
            row = [];
            for (j = 0; j < 7; j++)
            {
                row.push(data[i].children[j].innerText);
            }
            table.table.body.push(row);

        }

        dd.content.push(table);
        pdfMake.createPdf(dd).download('File.pdf');
    }

</script>
    </div>
</div>
</div>


<!--===============================================================================================-->
<script src="../vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="../vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
<script src="../vendor/bootstrap/js/popper.js"></script>
<script src="../vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="../vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
<script src="../vendor/daterangepicker/moment.min.js"></script>
<script src="../vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
<script src="../vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
<script src="../js/main.js"></script>

</body>
</html>