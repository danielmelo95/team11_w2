<!DOCTYPE html>
<html lang="en">
<head>
    <title>WEBTE2 | Zadanie 3</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="../images/icons/favicon.ico"/>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../fonts/iconic/css/material-design-iconic-font.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/animsition/css/animsition.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/daterangepicker/daterangepicker.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../css/util.css">
    <link rel="stylesheet" type="text/css" href="../css/main.css">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <!--===============================================================================================-->
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <meta name="google-signin-scope" content="profile email">
    <meta name="google-signin-client_id" content="826090786652-7uu82dad1i2hs3olt99djahd65nk1ikg.apps.googleusercontent.com">
    <!--===============================================================================================-->

</head>
<body>
<!-- ======================================NAVBAR============================================================== -->
   <?php
     session_start();
   if(!isset($_SESSION) || $_SESSION['type']!=4){
        header('Location: noPermission.php');

   }
   require ("navBar.php");
   echo generateNavbarAdmin();
   ?>
<!-- ======================================NAVBAR============================================================== -->
<style>
    .csv-btn:hover{
        background-color: #d7ffff;
        transition: 1s;
    }
</style>

<?php
    session_start();
?>

<div class="limiter">
    <div class="container-login100" style="background-image: url('../images/bg-01.jpg');">
        <div class="wrap-login100 p-l-55 p-r-55 p-t-65 p-b-54 w-50" style="margin-top: 4%">

<form class="login100-form validate-form" action="defineRouteAdmin.php" method="post" name="create">
                    <span class="login100-form-title p-b-49">
                        Define Route
                    </span>

                <div class="wrap-input100 validate-input m-b-23" data-validate = "Start latitude is reauired">
                    <span class="label-input100">GPS LAT start</span>
                    <input class="input100" type="text" name="GLatStart" placeholder="Enter starting latitude">
                    <span class="focus-input100" data-symbol="&#xf206;"></span>
                </div>
                <div class="wrap-input100 validate-input m-b-23" data-validate = "Start Longitude is required">
                    <span class="label-input100">GPS LONG start</span>
                    <input class="input100" type="text" name="GLongStart" placeholder="Enter starting longitude">
                    <span class="focus-input100" data-symbol="&#xf206;"></span>
                </div>

                <div class="wrap-input100 validate-input" data-validate="End latitude is required">
                    <span class="label-input100">GPS LAT stop</span>
                    <input class="input100" type="text" name="GLatStop" placeholder="Enter end latitude">
                    <span class="focus-input100" data-symbol="&#xf190;"></span>
                </div>
                <br>

                <div class="wrap-input100 validate-input" data-validate="End longitude is required">
                    <span class="label-input100">GPS LONG stop</span>
                    <input class="input100" type="text" name="GLongStop" placeholder="Enter end longitude">
                    <span class="focus-input100" data-symbol="&#xf190;"></span>
                </div>
                <br>
                <span class="label-input100">Route mode</span>
                <select name="routeType"  class=" form-control" onchange="typeChange(this.selectedIndex);" >
                      <option disabled selected value> -- select an option -- </option>
                      <option value="public">Public mode</option>
                      <option value="relay">Relay mode</option>
                </select> 
                <br>

                <?php //render of Select menu for user when selecting public route
                echo '<div hidden id = "user" >';
                echo '<span class="label-input100">User</span>';
                echo '<select name="user" id="user" class=" form-control">';
                        require('config.php'); 
                        $query = "SELECT * FROM `User`";
                        $connection = mysqli_connect($login_servername,$login_username,$login_password,$login_dbname);
                        $connection->set_charset("UTF8");
                        $result = mysqli_query($connection, $query);
                        echo '<option disabled selected value> -- select an option -- </option>';
                        while( $row = $result->fetch_assoc() )
                            echo '<option value="'.$row['id'].'">'.$row['first_name'].' '.$row['last_name'] .' - '. $row['address'].'</option>';
                        mysqli_free_result($result);
                        $connection->close();
                echo '</select><br>';
                echo '</div>';
                ?>

                 <?php //render of Select menu for group  when selecting relay route
                echo '<div hidden id = "group" >';
                echo '<span class="label-input100">Group</span>';
                echo '<select name="group" id="group" class=" form-control">';
                        require('config.php'); 
                        $query = "SELECT * FROM `Group` ";
                        $connection = mysqli_connect($login_servername,$login_username,$login_password,$login_dbname);
                        $connection->set_charset("UTF8");
                        $result = mysqli_query($connection, $query);
                        echo '<option disabled selected value> -- select an option -- </option>';
                        while( $row = $result->fetch_assoc() )
                            echo '<option value="'.$row['id'].'">'.$row['name'].'</option>';
                        mysqli_free_result($result);
                        $connection->close();
                echo '</select><br>';
                echo '</div>';
                ?>

                <div class="container-login100-form-btn">
                    <div class="wrap-login100-form-btn">
                        <div class="login100-form-bgbtn"></div>
                        <button type="submit" class="login100-form-btn" name="create">
                            Create route
                        </button>
                    </div>
                </div>



            </form>
                

        </div>

    </div>

</div>

<?php
	require('config.php');
	// $id = $_SESSION["id/"];
	if(isset($_POST["create"]))
	{
		$GLatStart = $_POST["GLatStart"];
		$GLongStart = $_POST["GLongStart"];
		$GLatStop = $_POST["GLatStop"];
		$GLongStop = $_POST["GLongStop"];
        $routeType = $_POST['routeType'];
        $userForm = $_POST['user'];
        $groupForm = $_POST['group'];
        
        // echo $routeType."<br>";
        // echo $userForm."<br>";
        // echo $groupForm."<br>";

        if(strcmp($routeType,"public")==0){
            $type =(int)2;
            $sql = "INSERT INTO Route (active,  start_lat, start_long, end_lat, end_long, type, `user`)
                    VALUES (1, ".$GLatStart  ." , ". $GLongStart .", ". $GLatStop .", ". $GLongStop .", ".$type.",".$_POST['user'].");";
	
        }
        if(strcmp($routeType,"relay")==0){
            $type = (int)3;
            $sql = "INSERT INTO Route (active,  start_lat, start_long, end_lat, end_long, type,`group`)
					VALUES (1, ".$GLatStart  ." , ". $GLongStart .", ". $GLatStop .", ". $GLongStop .", ".$type.",".$_POST['group'].");";
	
        }

		$conn = new mysqli($login_servername,$login_username,$login_password,$login_dbname);

		if ($conn->query($sql) === TRUE) {
			// echo "New record created successfully";
		} else {
		echo "Error: " . $sql . "<br>" . $conn->error;
		}

		$conn->close();
		
	}

?>

<script>
function typeChange(value) {
    var user = document.getElementById("user");
    var group = document.getElementById("group");

    if (value == 1)
    {
        user.removeAttribute("hidden");
        group.setAttribute("hidden", true);
    }
    if (value == 2)
    {
        group.removeAttribute("hidden");
        user.setAttribute("hidden", true);
    }
}

</script>
<!--===============================================================================================-->
<script src="../vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="../vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
<script src="../vendor/bootstrap/js/popper.js"></script>
<script src="../vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="../vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
<script src="../vendor/daterangepicker/moment.min.js"></script>
<script src="../vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
<script src="../vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
<script src="../js/main.js"></script>

</body>
</html>