<!DOCTYPE html>
<html lang="en">
<head>
    <title>WEBTE2 | Zadanie 3</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="../images/icons/favicon.ico"/>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../fonts/iconic/css/material-design-iconic-font.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/animsition/css/animsition.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/daterangepicker/daterangepicker.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../css/util.css">
    <link rel="stylesheet" type="text/css" href="../css/main.css">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <!--===============================================================================================-->
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <meta name="google-signin-scope" content="profile email">
    <meta name="google-signin-client_id" content="826090786652-7uu82dad1i2hs3olt99djahd65nk1ikg.apps.googleusercontent.com">
    <!--===============================================================================================-->

</head>
<body>
    <!-- ======================================NAVBAR============================================================== -->

    <?php
       require ("navBar.php");
       echo generateNavbarUser();
   ?>
<!-- ======================================NAVBAR============================================================== -->    
<style>
    .csv-btn:hover{
        background-color: #d7ffff;
        transition: 1s;
    }
</style>

<?php
    session_start();
?>

    <div class="limiter">
        <div class="container-login100" style="background-image: url('../images/bg-01.jpg');">
            <div class="wrap-login100 p-l-55 p-r-55 p-t-65 p-b-54 " style="width: 70%; text-align: center; margin-top: 5%">

<form class="login100-form validate-form" action="defineRouteUser.php" method="post" name="create">
                    <span class="login100-form-title p-b-49">
                        Define Route
                    </span>

                <div class="wrap-input100 validate-input m-b-23" data-validate = "Latitude of start is required">
                    <span class="label-input100">GPS LAT start</span>
                    <input class="input100" type="text" name="GLatStart" placeholder="Latitude of start point">
                    <span class="focus-input100" data-symbol="&#xf206;"></span>
                </div>
                <div class="wrap-input100 validate-input m-b-23" data-validate = "Longitude of start is required">
                    <span class="label-input100">GPS LONG start</span>
                    <input class="input100" type="text" name="GLongStart" placeholder="Longitude of start">
                    <span class="focus-input100" data-symbol="&#xf206;"></span>
                </div>

                <div class="wrap-input100 validate-input" data-validate="Latitude of end point">
                    <span class="label-input100">GPS LAT stop</span>
                    <input class="input100" type="text" name="GLatStop" placeholder="Latitude of end point">
                    <span class="focus-input100" data-symbol="&#xf190;"></span>
                </div>
                <br>

                <div class="wrap-input100 validate-input" data-validate="Longitude of end point">
                    <span class="label-input100">GPS LONG stop</span>
                    <input class="input100" type="text" name="GLongStop" placeholder="Longitude of end point">
                    <span class="focus-input100" data-symbol="&#xf190;"></span>
                </div>
                <br>
                <br>

                <div class="container-login100-form-btn">
                    <div class="wrap-login100-form-btn">
                        <div class="login100-form-bgbtn"></div>
                        <button type="submit" class="login100-form-btn" name="create">
                            Create route
                        </button>
                    </div>
                </div>



            </form>
                

        </div>

    </div>

</div>

<?php
	require('../config.php');
	$id = $_SESSION["id"];
	if(isset($_POST["create"]))
	{
		$GLatStart = $_POST["GLatStart"];
		$GLongStart = $_POST["GLongStart"];
		$GLatStop = $_POST["GLatStop"];
		$GLongStop = $_POST["GLongStop"];
		
		$conn = new mysqli($servername,$username,$password,$dbname);
		
		$sqlUpdate = "UPDATE Route
						SET active = 0
						WHERE user = ". $id .";";
						
		$conn->query($sqlUpdate);
		
		$sql = "INSERT INTO Route (active, user, start_lat, start_long, end_lat, end_long, type)
					VALUES (1, ". $id .",".$GLatStart  ." , ". $GLongStart .", ". $GLatStop .", ". $GLongStop .", 1);";
		
		if ($conn->query($sql) === TRUE) {
			// echo "New record created successfully";
		} else {
		// echo "Error: " . $sql . "<br>" . $conn->error;
		}

		$conn->close();
		
	}

?>


<!--===============================================================================================-->
<script src="../vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="../vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
<script src="../vendor/bootstrap/js/popper.js"></script>
<script src="../vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="../vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
<script src="../vendor/daterangepicker/moment.min.js"></script>
<script src="../vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
<script src="../vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
<script src="../js/main.js"></script>

</body>
</html>