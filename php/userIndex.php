<!DOCTYPE html>
<html lang="en">
<head>
    <title>WEBTE2 | Zadanie 3</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="../images/icons/favicon.ico"/>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../fonts/iconic/css/material-design-iconic-font.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/animsition/css/animsition.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/daterangepicker/daterangepicker.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../css/util.css">
    <link rel="stylesheet" type="text/css" href="../css/main.css">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <!--===============================================================================================-->
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <meta name="google-signin-scope" content="profile email">
    <meta name="google-signin-client_id" content="826090786652-7uu82dad1i2hs3olt99djahd65nk1ikg.apps.googleusercontent.com">
    <!--===============================================================================================-->
    <script src="../js/sorttable.js"></script>
    <script src='../js/pdfmake.min.js'></script>
    <script src='../js/vfs_fonts.js'></script>
    <!--===============================================================================================-->

</head>
<body>
                   
<!-- ======================================NAVBAR============================================================== -->

<?php
   require ("navBar.php");
   echo generateNavbarUser();
   ?>
<!-- ======================================NAVBAR============================================================== -->                  
<style>
    .csv-btn:hover{
        background-color: #d7ffff;
        transition: 1s;
    }
</style>

<div class="limiter">
    <div class="container-login100" style="background-image: url('../images/bg-01.jpg');">
        <div class="wrap-login100 p-l-55 p-r-55 p-t-65 p-b-54 w-50" style="margin-top: 4%">

            <span class="login100-form-title p-b-49">
                User index
            </span>
            Welcome 
                <?php
                session_start();
                require('../config.php');
                    $connection = mysqli_connect($servername,$username,$password,$dbname);
                    $connection->set_charset("UTF8");
                    $query = "SELECT email FROM User WHERE id = '".$_SESSION['id']."'";                
                    $result = mysqli_query($connection, $query);
                    $res = $result->fetch_assoc();
                    echo $res['email'].'.<br><br>';
                ?>
                    If you want to create route for yourself, define run or see all your performances please click on the <span class="fa fa-bars"></span> icon.
                    <br><br>
                    By clicking on <span class="fa fa-home"></span> button you will be redirected to home page and logged off.
                    <br><br>
            <!-- <div class="flex-c-m">
                <a href="defineRoute.php" class="">
                    Define route
                </a>
            </div>

            <div class="flex-c-m">
                <a href="defineRun.php" class="">
                    Define run
                </a>
            </div>
            <div class="flex-c-m">
                <a href="perfTable.php" class="">
                Performance Table
                </a>
            </div> -->
                
        <!-- TODO preview vsetkych tras usera, user id je ulozenie v $_SESSION['id'] -->
        </div>

    </div>

</div>


<!--===============================================================================================-->
<script src="../vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="../vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
<script src="../vendor/bootstrap/js/popper.js"></script>
<script src="../vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="../vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
<script src="../vendor/daterangepicker/moment.min.js"></script>
<script src="../vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
<script src="../vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
<script src="../js/main.js"></script>

</body>
</html>