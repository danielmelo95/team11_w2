<!DOCTYPE html>
<html lang="en">
<head>
    <title>WEBTE2 | Finálne zadanie</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="../images/icons/favicon.ico"/>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../fonts/iconic/css/material-design-iconic-font.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/animsition/css/animsition.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/daterangepicker/daterangepicker.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../css/util.css">
    <link rel="stylesheet" type="text/css" href="../css/main.css">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <!--===============================================================================================-->
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <meta name="google-signin-scope" content="profile email">
    <meta name="google-signin-client_id" content="826090786652-7uu82dad1i2hs3olt99djahd65nk1ikg.apps.googleusercontent.com">
    <!--===============================================================================================-->

</head>
<body>
    <!-- ======================================NAVBAR============================================================== -->
         
   <?php
     session_start();

   include "navBar.php";
   generateNavbarLogin();
?>
<!-- ======================================NAVBAR============================================================== -->

<?php
session_start();

require '../config.php';
$mysqli = new mysqli($hostname,$username,$password,$dbname);

if(isset($_POST['login'])){
    // echo $_POST['pass'].'------';
    $result = $mysqli->query("SELECT type, id FROM Final.User WHERE email = '".$_POST['mailAddr']."' AND password = '".hash('sha512', $_POST['pass'])."';");
    $res1 = $result->fetch_assoc();

    // echo $res1['type'];


    if(!empty($res1['type'])) {
        // echo $res1['type'];
        $type = $res1['type'];
        if($type == 4){
            // echo 'fiejafjlfjw';
            $_SESSION['id'] = $res1['id'];
            $_SESSION['type'] = $res1['type']; 
            unset($_POST);
            // $_SESSION['type'] = $res1['type']; 
            header('Location: adminIndex.php');
        }
        if($type == 1){
            // echo 'good user';
            $_SESSION['id'] = $res1['id'];
            $_SESSION['type'] = $res1['type']; 
            header('Location: userIndex.php');

            //TODO doplnit stranku pre riadne registrovaneho uzivatela
        }
        if($type == 2){
            $_SESSION['id'] = $res1['id'];
            $_SESSION['type'] = $res1['type']; 
            echo 'plz validate';
            header('Location: validateUser.php');

            //TODO doplnit stranku pre validaciu
        }
        if($type == 3){
            echo 'bulk regstraitor';
            $_SESSION['id'] = $res1['id'];
            $_SESSION['type'] = $res1['type']; 
            header('Location: changePassword.php');

            //TODO straku pre hromadne registrovaneho 
        }

    }
    else{
        // echo 'djasj';

         echo "<div class='alert' style='margin: 0; padding: 20px; background-color: #f44336; color: white;'>
            Incorrect username or password, please try again!
            </div>";
        
    }
    // -- if($mysqli->query("SELECT type FROM Final.User WHERE email = '".hash('sha512', $_POST['mailAddr'])."';")){
    // $result = $mysqli->query("SELECT type FROM Final.User WHERE email = '".$_POST['mailAddr']."';");
    // echo 'heh';
// }
}

?>

<div class="limiter">
    <div class="container-login100" style="background-image: url('../images/bg-01.jpg');">
        <div class="wrap-login100 p-l-55 p-r-55 p-t-65 p-b-54">
            <form class="login100-form validate-form" action="adminLogin.php" method="post" name="login">
                    <span class="login100-form-title p-b-49">
                        Login
                    </span>

                <div class="wrap-input100 validate-input m-b-23" data-validate = "Username is reauired">
                    <span class="label-input100">E-mail adress</span>
                    <input class="input100" type="text" name="mailAddr" placeholder="Type your username">
                    <span class="focus-input100" data-symbol="&#xf206;"></span>
                </div>

                <div class="wrap-input100 validate-input" data-validate="Password is required">
                    <span class="label-input100">Password</span>
                    <input class="input100" type="password" name="pass" placeholder="Type your password">
                    <span class="focus-input100" data-symbol="&#xf190;"></span>
                </div>
                <br>
                <br>

                <div class="container-login100-form-btn">
                    <div class="wrap-login100-form-btn">
                        <div class="login100-form-bgbtn"></div>
                        <button type="submit" class="login100-form-btn" name="login">
                            Login
                        </button>
                    </div>
                </div>


                <div class="txt1 text-center p-t-54 p-b-20">
                        <span>
                            Or

                            <div class="flex-c-m">
                        <a href="loginEmail.php" class=""> 
                            <!-- TODO doplnit link -->
                            Sign Up
                        </a>
                        </span>
                </div>


            </form>

        </div>

    </div>

</div>




<!--===============================================================================================-->
<script src="../vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="../vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
<script src="../vendor/bootstrap/js/popper.js"></script>
<script src="../vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="../vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
<script src="../vendor/daterangepicker/moment.min.js"></script>
<script src="../vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
<script src="../vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
<script src="../js/main.js"></script>

</body>
</html>