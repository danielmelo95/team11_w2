<?php
function generateNavbarAdmin()
{
    
 return '<style >
        
#feedback {
    position: fixed;
    left: 10px;
    top: 25px;
    z-index: 1000;
   
}
#feedback a {
    display: block;
    background:rgba(51, 51, 51, 0.8);
    height: 50px;
    padding-top: 7px;
    width: 50px;
    text-align: center;
    color: #fff;

}
#feedback a:hover {
    background:#00495d;
}

    .csv-btn:hover{
        background-color: #d7ffff;
        transition: 1s;
    }

    nav{
        background-color: white;

    }
    .nav-item {
        background-color: white;
    }
    
</style>


<nav class="navbar navbar-light light-blue lighten-4 w-100" style="position: fixed;">

 <div id="feedback navbar-brand" >
        <a href="../index.php" class="fa fa-home"> </a>
    </div>
    <button class="navbar-toggler toggler-example" type="button" data-toggle="collapse" data-target="#navbarSupportedContent1" aria-controls="navbarSupportedContent1"
        aria-expanded="false" aria-label="Toggle navigation"><span class="dark-blue-text"><i class="fa fa-bars fa-1x"></i></span></button>

    <div class="collapse navbar-collapse w-100" id="navbarSupportedContent1">

        <ul class="navbar-nav mr-auto ">

            <li class="nav-item w-100">
                <a href="adminIndex.php" class="w-100">
                        Admin Home
                    </a>            
            </li>
            <li class="nav-item w-100">
                <a href="groupManagementAdmin.php" class="w-100">
                        Group Management
                    </a>            
            </li>
            <li class="nav-item">
                <a href="defineRouteAdmin.php" class="">
                        Create Route
                    </a>            
            </li>
            <li class="nav-item">
                <a href="usersPreviewAdmin.php" class="">
                        Preview All Users
                    </a>            
            </li>
            <li class="nav-item">
                <a href="groupsPreviewAdmin.php" class="">
                        Preview All Groups
                </a>            
            </li>
            <li class="nav-item w-100">
                <a href="Newsletter.php" class="w-100">
                        Add news for newsletter
                    </a>            
            </li>
        </ul>

    </div>

</nav>
  ';              
                    
}
function generateNavbarUser(){
 return '<style >
        
#feedback {
    position: fixed;
    left: 10px;
    top: 25px;
    z-index: 1000;
   
}
#feedback a {
    display: block;
    background:rgba(51, 51, 51, 0.8);
    height: 50px;
    padding-top: 7px;
    width: 50px;
    text-align: center;
    color: #fff;

}
#feedback a:hover {
    background:#00495d;
}

    .csv-btn:hover{
        background-color: #d7ffff;
        transition: 1s;
    }

    nav{
        background-color: white;

    }
    .nav-item {
        background-color: white;
    }
    
</style>


<nav class="navbar navbar-light light-blue lighten-4 w-100" style="position: fixed;">

 <div id="feedback navbar-brand" >
        <a href="../index.php" class="fa fa-home"> </a>
    </div>
    <button class="navbar-toggler toggler-example" type="button" data-toggle="collapse" data-target="#navbarSupportedContent1" aria-controls="navbarSupportedContent1"
        aria-expanded="false" aria-label="Toggle navigation"><span class="dark-blue-text"><i class="fa fa-bars fa-1x"></i></span></button>

    <div class="collapse navbar-collapse w-100" id="navbarSupportedContent1">

        <ul class="navbar-nav mr-auto ">
            <li class="nav-item w-100">
                <a href="userIndex.php" class="">
                    User Home
                </a>           
            </li>
            <li class="nav-item w-100">
                <a href="defineRouteUser.php" class="">
                    Define route
                </a>           
            </li>
            <li class="nav-item">
                <a href="defineRunUser.php" class="">
                    Define run
                </a>         
            </li>
            <li class="nav-item">
                <a href="perfTable.php" class="">
                Performance Table
                </a>         
            </li>
            <li class="nav-item w-100">
                <a href="News.php" class="">
                    News
                </a>           
            </li>
        </ul>

    </div>

</nav>
  ';  }
function generateNavbarLogin(){
    
 echo '   <style>
        
#feedback {

    position: fixed;
    left: 10px;
    top: 25px;
    z-index: 1000;

}
#feedback a {
    display: block;
    background:rgba(51, 51, 51, 0.8);
    height: 50px;
    padding-top: 7px;
    width: 50px;
    text-align: center;
    color: #fff;

}
#feedback a:hover {
    background:#00495d;
}

    .csv-btn:hover{
        background-color: #d7ffff;
        transition: 1s;
    }

    nav{
        background-color: white;

    }
    .nav-item {
        background-color: white;
    }
    
</style>


<nav class="navbar navbar-light light-blue lighten-4 w-100" style="position: fixed;">

 <div id="feedback navbar-brand" >
        <a href="../index.php" class="fa fa-home"> </a>
    </div>

   

</nav>';
}

?>