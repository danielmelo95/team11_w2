<!DOCTYPE html>
<html lang="en">
<head>
    <title>WEBTE2 | Zadanie 3</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="../images/icons/favicon.ico"/>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../fonts/iconic/css/material-design-iconic-font.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/animsition/css/animsition.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/daterangepicker/daterangepicker.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../css/util.css">
    <link rel="stylesheet" type="text/css" href="../css/main.css">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <!--===============================================================================================-->
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <meta name="google-signin-scope" content="profile email">
    <meta name="google-signin-client_id" content="826090786652-7uu82dad1i2hs3olt99djahd65nk1ikg.apps.googleusercontent.com">
    <!--===============================================================================================-->

</head>
<body>
        <!-- ======================================NAVBAR============================================================== -->

    <?php
       require ("navBar.php");
       echo generateNavbarUser();
   ?>
<!-- ======================================NAVBAR============================================================== -->    
<style>
    .csv-btn:hover{
        background-color: #d7ffff;
        transition: 1s;
    }
</style>

<?php
    session_start();
?>

    <div class="limiter">
        <div class="container-login100" style="background-image: url('../images/bg-01.jpg');">
            <div class="wrap-login100 p-l-55 p-r-55 p-t-65 p-b-54 " style="width: 70%; text-align: center; margin-top: 5%">

<!-- TODO upravit formular pre define run  -->

<form class="login100-form validate-form" action="defineRunUser.php" method="post" name="create">
                    <span class="login100-form-title p-b-49">
                        Define Run
                    </span>

                <div class="wrap-input100 m-b-23" data-validate = "Field is required">
                    <span class="label-input100">GPS LAT start</span>
                    <input class="input100" type="text" name="GLatStart" placeholder="Here goes latitude of your start">
                    <span class="focus-input100" data-symbol="&#xf206;"></span>
                </div>
                <div class="wrap-input100 m-b-23" data-validate = "Field is required">
                    <span class="label-input100">GPS LONG start</span>
                    <input class="input100" type="text" name="GLongStart" placeholder="Here goes longitude of your start">
                    <span class="focus-input100" data-symbol="&#xf206;"></span>
                </div>

                <div class="wrap-input100" >
                    <span class="label-input100">GPS LAT stop</span>
                    <input class="input100" type="text" name="GLatStop" placeholder="Here goes latitude of your finish">
                    <span class="focus-input100" data-symbol="&#xf190;"></span>
                </div>
                <br>

                <div class="wrap-input100" >
                    <span class="label-input100">GPS LONG stop</span>
                    <input class="input100" type="text" name="GLongStop" placeholder="Here goes longitude of your finish">
                    <span class="focus-input100" data-symbol="&#xf190;"></span>
                </div>
                <br>
				
				<div class="wrap-input100 validate-input" data-validate="Field is required">
                    <span class="label-input100">Distance</span>
                    <input class="input100" type="text" name="Distance" placeholder="Distance">
                    <span class="focus-input100" data-symbol="&#xf190;"></span>
                </div>
                <br>
				
				<div class="wrap-input100 " >
                    <span class="label-input100">Date</span>
                    <input class="input100" type="date" name="datum" placeholder="Choose date">
                    <span class="focus-input100" data-symbol="&#xf190;"></span>
                </div>
                <br>
				
				<div class="wrap-input100" >
                    <span class="label-input100">Start time</span>
                    <input class="input100" type="text" name="StartTime" placeholder="HH:MM:SS">
                    <span class="focus-input100" data-symbol="&#xf190;"></span>
                </div>
                <br>
				
				<div class="wrap-input100" >
                    <span class="label-input100">End time</span>
                    <input class="input100" type="text" name="EndTime" placeholder="HH:MM:SS">
                    <span class="focus-input100" data-symbol="&#xf190;"></span>
                </div>
                <br>
				
				<div class="wrap-input100" >
                    <span class="label-input100">Rating</span>
                    <input class="input100" type="number" name="Rating" placeholder="1-5">
                    <span class="focus-input100" data-symbol="&#xf190;"></span>
                </div>
                <br>
				
				<div class="wrap-input100 " >
                    <span class="label-input100">Note</span>
                    <input class="input100" type="text" name="Note" placeholder="Type your note">
                    <span class="focus-input100" data-symbol="&#xf190;"></span>
                </div>
                <br>
                <br>

                <div class="container-login100-form-btn">
                    <div class="wrap-login100-form-btn">
                        <div class="login100-form-bgbtn"></div>
                        <button type="submit" class="login100-form-btn" name="create">
                            Create RUN
                        </button>
                    </div>
                </div>



            </form>
                

        </div>

    </div>

</div>
<?php
	require('../config.php');
	$id = $_SESSION["id"];
	if(isset($_POST["create"]))
	{
		$GLatStart = $_POST["GLatStart"];
		$GLongStart = $_POST["GLongStart"];
		$GLatStop = $_POST["GLatStop"];
		$GLongStop = $_POST["GLongStop"];
		$Distance = $_POST["Distance"];
		$date = $_POST["datum"];
		//$date = mysql_real_escape_string($_POST['datum']);
		//$date = date('Y-m-d', strtotime(str_replace('-', '/', $date)));
		$StartTime = $_POST["StartTime"];
		$EndTime = $_POST["EndTime"];
		$Rating = $_POST["Rating"];
		$Note = $_POST["Note"];
        // echo $date;
        // echo date("d-m-Y", strtotime($date));
		
        $conn = new mysqli($servername,$username,$password,$dbname);
		
		$sqlFindID = "SELECT id
						FROM Route
						WHERE user = ". $id ." AND active=1;";
						
		$result = $conn->query($sqlFindID);
		if($result->num_rows > 0) {
			$row = $result->fetch_assoc();
			$idRoute = $row["id"];
			$sql = "INSERT INTO Run (route, start_lat, start_long, end_lat, end_long, distance, date, start_time, end_time, rating, note)
						VALUES (". $idRoute .",".$GLatStart  ." , ". $GLongStart .", ". $GLatStop .", ". $GLongStop .", ". $Distance .", '". $date ."', '". $StartTime ."','". $EndTime ."', ". $Rating .",'". $Note ."');";
			// echo $sql;
			if ($conn->query($sql) === TRUE) {
				// echo "New record created successfully";
			} else {
			// echo "Error: " . $sql . "<br>" . $conn->error;
			}

			$conn->close();
		}
	}

?>

<!--===============================================================================================-->
<script src="../vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="../vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
<script src="../vendor/bootstrap/js/popper.js"></script>
<script src="../vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="../vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
<script src="../vendor/daterangepicker/moment.min.js"></script>
<script src="../vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
<script src="../vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
<script src="../js/main.js"></script>

</body>
</html>