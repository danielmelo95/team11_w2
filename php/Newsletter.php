<!DOCTYPE html>
<html lang="en">
<head>
    <title>WEBTE2 | Zadanie 3</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="../images/icons/favicon.ico"/>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../fonts/iconic/css/material-design-iconic-font.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/animsition/css/animsition.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/daterangepicker/daterangepicker.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../css/util.css">
    <link rel="stylesheet" type="text/css" href="../css/main.css">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <!--===============================================================================================-->
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <meta name="google-signin-scope" content="profile email">
    <meta name="google-signin-client_id" content="826090786652-7uu82dad1i2hs3olt99djahd65nk1ikg.apps.googleusercontent.com">
    <!--===============================================================================================-->

</head>
<body>
    <!-- ======================================NAVBAR============================================================== -->

    <?php
       session_start();
    // var_dump($_SESSION);

   if(!isset($_SESSION['type']) || $_SESSION['type']!=4){
        header('Location: noPermission.php');

   }
       require ("navBar.php");
       echo generateNavbarAdmin();
   ?>
<!-- ======================================NAVBAR============================================================== -->   
<style>
    .csv-btn:hover{
        background-color: #d7ffff;
        transition: 1s;
    }
</style>

<?php
    session_start();
?>

    <div class="limiter">
        <div class="container-login100" style="background-image: url('../images/bg-01.jpg');">
            <div class="wrap-login100 p-l-55 p-r-55 p-t-65 p-b-54 " style="width: 70%; margin-top: 5%">

<form class="login100-form validate-form" action="Newsletter.php" method="post" name="create">
                    <span class="login100-form-title p-b-49">
                        Create news for newsletter
                    </span>

                <div class="wrap-input100 validate-input m-b-23" data-validate = "Title is required">
                    <span class="label-input100">Heading</span>
                    <input class="input100" type="text" name="Nadpis" placeholder="Title">
                    <span class="focus-input100" data-symbol="&#xf206;"></span>
                </div>
                <div class="wrap-input100 validate-input m-b-23" data-validate = "Text is required">
                    <span class="label-input100">Text</span>
                    <textarea rows="4" cols="50" wrap="hard" class="input100" type="text" name="Text" placeholder="Text"></textarea>
                    <!-- <span class="focus-input100" data-symbol="&#xf206;"></span> -->
                </div>

                
                <br>
                <br>

                <div class="container-login100-form-btn">
                    <div class="wrap-login100-form-btn">
                        <div class="login100-form-bgbtn"></div>
                        <button type="submit" class="login100-form-btn" name="create">
                            Create News
                        </button>
                    </div>
                </div>



            </form>
                

        </div>

    </div>

</div>

<?php
	require('../config.php');
	$id = $_SESSION["id"];
	if(isset($_POST["create"]))
	{
		//$obsah = str_replace(PHP_EOL, "<br>", $_POST["Text"]);
		$obsah = trim(preg_replace('/\s+/', '<br>', $_POST["Text"]));
		$mailServer = "rand.acc.666@gmail.com";     // this is your Email address
		
		$sql = "SELECT email FROM User
				WHERE newsletter=1;";
		
		$conn = new mysqli($servername,$username,$password,$dbname);
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
				$userEmail = $row['email'];               // this is the sender's Email address
				//News e-mail
				$headers = "From:" . $mailServer;
				$subject = $_POST["Nadpis"];
				$message = $_POST["Text"];
				mail($userEmail,$subject,$message,$headers);
			}
		}
		
		$newsFile = fopen("../txt/News.txt", "r");
		$text = fread($newsFile,filesize("../txt/News.txt"));
		fclose($newsFile);
		
		
		$newsFile = fopen("../txt/News.txt", "r+");
		$text = "<tr><th><h3> " . $_POST["Nadpis"] . "</h3><br><p>" . $obsah . "</p></th><th>". date("d.m.Y") ."</th></tr>" . $text;
		fwrite($newsFile,$text);
		fclose($newsFile);
		
		/*$newsFile = fopen("News.txt", "r");
		echo fread($newsFile,filesize("News.txt"));
		fclose($newsFile);*/
	}

?>


<!--===============================================================================================-->
<script src="../vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="../vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
<script src="../vendor/bootstrap/js/popper.js"></script>
<script src="../vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="../vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
<script src="../vendor/daterangepicker/moment.min.js"></script>
<script src="../vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
<script src="../vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
<script src="../js/main.js"></script>

</body>
</html>