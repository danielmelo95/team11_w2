<!DOCTYPE html>
<html lang="en">
<head>
    <title>WEBTE2 | Zadanie 3</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="../images/icons/favicon.ico"/>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../fonts/iconic/css/material-design-iconic-font.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/animsition/css/animsition.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/daterangepicker/daterangepicker.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../css/util.css">
    <link rel="stylesheet" type="text/css" href="../css/main.css">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <!--===============================================================================================-->
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <meta name="google-signin-scope" content="profile email">
    <meta name="google-signin-client_id" content="826090786652-7uu82dad1i2hs3olt99djahd65nk1ikg.apps.googleusercontent.com">
    <!--===============================================================================================-->

</head>
<body>
     <!-- ======================================NAVBAR============================================================== -->
         
   <?php
   include "navBar.php";
   generateNavbarLogin();
?>
<!-- ======================================NAVBAR============================================================== -->
<style>
    .csv-btn:hover{
        background-color: #d7ffff;
        transition: 1s;
    }
</style>
<!-- ===================================PHP================================================================== -->
<?php
    session_start();
    echo $_SESSION['id'];
    $user_id =  $_SESSION['id'];
    require('../config.php');
    $mysqli = new mysqli($hostname,$username,$password,$dbname);

    if(isset($_POST['change'])){
    // echo 'heh';
        // echo $_POST['new_pass'];
    $hash_pass = hash('sha512', $_POST['new_pass']);
    // echo $hash_pass;
    $result = $mysqli->query("UPDATE `User` SET type = 1 WHERE id = $user_id");
    $result = $mysqli->query("UPDATE `User` SET password = '".$hash_pass."' WHERE id = $user_id");
    $_SESSION['type'] = 1;
    header('Location: userIndex.php');
}
?>
<!-- ======================================================================================================== -->

<div class="limiter">
    <div class="container-login100" style="background-image: url('../images/bg-01.jpg');">
        <div class="wrap-login100 p-l-55 p-r-55 p-t-65 p-b-54 w-50">


            <form class="login100-form validate-form" action="changePassword.php" method="post" name="change">
                    <span class="login100-form-title p-b-49">
                       Validation
                    </span>
                    <div style="text-align: center;">
                    <span class="label-input100" style="text-align: center;">You were registrated with deafult password, please change your password so you can continue</span>
                    </div>
                    <br>

                <div class="wrap-input100 validate-input m-b-23" data-validate = "Username is reauired">
                    <span class="label-input100">Old password</span>
                    <input class="input100" type="password" name="old_pass" placeholder="Here goes your old password">
                    <span class="focus-input100" data-symbol="&#xf206;"></span>
                </div>
                <div class="wrap-input100 validate-input m-b-23" data-validate = "Username is reauired">
                    <span class="label-input100">New password</span>
                    <input class="input100" type="password" name="new_pass" placeholder="Here goes your new password">
                    <span class="focus-input100" data-symbol="&#xf206;"></span>
                </div>

                <br>
                <br>

                <div class="container-login100-form-btn">
                    <div class="wrap-login100-form-btn w-50">
                        <div class="login100-form-bgbtn"></div>
                        <button type="submit" class="login100-form-btn" name="change">
                            change password
                        </button>
                    </div>
                </div>



            </form>


        </div>

    </div>

</div>


<!--===============================================================================================-->
<script src="../vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="../vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
<script src="../vendor/bootstrap/js/popper.js"></script>
<script src="../vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="../vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
<script src="../vendor/daterangepicker/moment.min.js"></script>
<script src="../vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
<script src="../vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
<script src="../js/main.js"></script>

</body>
</html>