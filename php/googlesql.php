<?php

//queryType: 1=School, 2 = personal
function returnAdresses($queryType){
	require('config.php');

	$conn = new mysqli($servername, $username, $password, $dbname);

	if (mysqli_connect_errno()) {
	    printf("Connect failed: %s\n", mysqli_connect_error());
	    exit();
	}
	$conn->set_charset("utf8");
	if($queryType == 2)
		$sql="SELECT address FROM Final.User";
	else
		$sql="SELECT address FROM Final.School";

	if ($result = $conn->query($sql)) {
		  /* fetch associative array */
	    while ($row = mysqli_fetch_assoc($result)) {
	        $a[]= ($row["address"]);
	    }
	    /* free result set */
	    mysqli_free_result($result);
	}

	$conn->close();
	$jsonOutput= json_encode($a);
	return $jsonOutput;
}

function returnCounts(){
	require('config.php');

	$conn = new mysqli($servername, $username, $password, $dbname);

	if (mysqli_connect_errno()) {
	    printf("Connect failed: %s\n", mysqli_connect_error());
	    exit();
	}
	$conn->set_charset("utf8");
	$sql="SELECT `School`.*, COUNT(`School`.`id`) as count FROM `School` JOIN `User` ON `School`.id = `User`.school Group BY `School`.id";
	if ($result = $conn->query($sql)) {
		  /* fetch associative array */
	    while ($row = mysqli_fetch_assoc($result)) {
	        $a[]= ($row["count"]);
	    }
	    /* free result set */
	    mysqli_free_result($result);
	}

	$conn->close();
	$jsonOutput= json_encode($a);
	return $jsonOutput;
}

//Type - string - DB column title
//return - column array
function returnCoordinates($type, $userID){
	require('config.php');

	$conn = new mysqli($servername, $username, $password, $dbname);

	if (mysqli_connect_errno()) {
	    printf("Connect failed: %s\n", mysqli_connect_error());
	    exit();
	}
	$conn->set_charset("utf8");
	$sql="SELECT ".$type." FROM `Route` JOIN `User` on `User`.id=`Route`.user WHERE active=1 AND `User`.id = ".$userID.";";

	if ($resultCoords = $conn->query($sql)) {
		  /* fetch associative array */
	    while ($row = mysqli_fetch_assoc($resultCoords)) {
        	$a[] = $row[$type];
	    }

	    /* free result set */
	    mysqli_free_result($resultCoords);
	}

	$conn->close();
	$jsonOutput= json_encode($a);
	return $jsonOutput;
}

?>
