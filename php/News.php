<!DOCTYPE html>
<html lang="en">
<head>
    <title>WEBTE2 | Zadanie 3</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="../images/icons/favicon.ico"/>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../fonts/iconic/css/material-design-iconic-font.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/animsition/css/animsition.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/daterangepicker/daterangepicker.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../css/util.css">
    <link rel="stylesheet" type="text/css" href="../css/main.css">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <!--===============================================================================================-->
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <meta name="google-signin-scope" content="profile email">
    <meta name="google-signin-client_id" content="826090786652-7uu82dad1i2hs3olt99djahd65nk1ikg.apps.googleusercontent.com">
    <!--===============================================================================================-->

</head>
<body>
    <!-- ======================================NAVBAR============================================================== -->

    <?php
       require ("navBar.php");
       echo generateNavbarUser();
   ?>
<!-- ======================================NAVBAR============================================================== -->   
<style>
    .csv-btn:hover{
        background-color: #d7ffff;
        transition: 1s;
    }
</style>

<?php
    session_start();
?>

    <div class="limiter">
        <div class="container-login100" style="background-image: url('../images/bg-01.jpg');">
            <div class="wrap-login100 p-l-55 p-r-55 p-t-65 p-b-54 " style="width: 70%; text-align: center; margin-top: 5%">

<form class="login100-form validate-form" action="News.php" method="post" name="create">
                    <span class="login100-form-title p-b-49">
                        News
                    </span>

                
				<?php
					$txt1 = "<div class=\"container-login100-form-btn\">
						<div class=\"wrap-login100-form-btn\">
							<div class=\"login100-form-bgbtn\"></div><input type=\"hidden\" name=\"odhlas\" value=\"aaaa\">
							<button type=\"submit\" class=\"login100-form-btn\" name=\"create\">
								cancel newsletter subscription
							</button>
						</div>
					</div>";
					
					$txt2 = "<div class=\"container-login100-form-btn\">
						<div class=\"wrap-login100-form-btn\">
							<div class=\"login100-form-bgbtn\"></div><input type=\"hidden\" name=\"prihlas\" value=\"aaaa\">
							<button type=\"submit\" class=\"login100-form-btn\" name=\"create\">
								subscribe for newsletter notifications
							</button>
						</div>
					</div>";
					
					require('../config.php');
					$id = $_SESSION["id"];
					
					$conn = new mysqli($servername,$username,$password,$dbname);
					
					$mainSql ="SELECT newsletter FROM User
								WHERE id=". $id .";";
								
					$result = $conn->query($mainSql);
					$row = $result->fetch_assoc();
					
					if($row["newsletter"] == 1)
					{
						echo $txt1;
					}else echo $txt2;
					
					
					if(isset($_POST["odhlas"]))
					{
						
						$sql = "UPDATE User
								SET newsletter = 0
								WHERE id=". $id .";";
								
						
						$conn->query($sql);
					}
					if(isset($_POST["prihlas"]))
					{
						$sql = "UPDATE User
								SET newsletter = 1
								WHERE id=". $id .";";
								
						
						$conn->query($sql);
					}
					
					
				?>

                
                <br>
                <br>

                



            </form>
             
			<?php
				
				echo "<table class='table'> <tr> <th>News</th><th>Date</th></tr>";
				$newsFile = fopen("../txt/News.txt", "r");
				echo fread($newsFile,filesize("../txt/News.txt"));
				fclose($newsFile);
				echo "</table>";
			?>


        </div>

    </div>

</div>




<!--===============================================================================================-->
<script src="../vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="../vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
<script src="../vendor/bootstrap/js/popper.js"></script>
<script src="../vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="../vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
<script src="../vendor/daterangepicker/moment.min.js"></script>
<script src="../vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
<script src="../vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
<script src="../js/main.js"></script>

</body>
</html>