<!DOCTYPE html>
<html lang="en">
<head>
    <title>WEBTE2 | Zadanie 3</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="../images/icons/favicon.ico"/>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../fonts/iconic/css/material-design-iconic-font.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/animsition/css/animsition.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/daterangepicker/daterangepicker.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../css/util.css">
    <link rel="stylesheet" type="text/css" href="../css/main.css">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <!--===============================================================================================-->
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <meta name="google-signin-scope" content="profile email">
    <meta name="google-signin-client_id" content="826090786652-7uu82dad1i2hs3olt99djahd65nk1ikg.apps.googleusercontent.com">
    <!--===============================================================================================-->

</head>
<body>    <!-- ======================================NAVBAR============================================================== -->
         
   <?php
     session_start();

   include "navBar.php";
   generateNavbarLogin();
?>
<!-- ======================================NAVBAR============================================================== -->
<style>
    .csv-btn:hover{
        background-color: #d7ffff;
        transition: 1s;
    }
</style>

<?php
    session_start();
?>

    <div class="limiter">
        <div class="container-login100" style="background-image: url('../images/bg-01.jpg');">
            <div class="wrap-login100 p-l-55 p-r-55 p-t-65 p-b-54 " style="width: 70%; text-align: center; margin-top: 5%">

<!-- TODO upravit formular pre define run  -->


                    <span class="login100-form-title p-b-49">
                        Technická dokumentácia
                    </span>

					<h1>Dokumentácia</h1>
					<h2>Analýza projektu a rozdelenie úloh</h2>
					<p>
					Prvé stretnutie nášho tímu sa konalo 14.4.2018. Na tomto stretnutí sa analyzoval projekt, aký je problém, aký systém je potrebné vyhotoviť. Spísali sme si aké oblasti, ktoré je potrebné zanalyzovať a rozdelili sme niektoré z týchto oblastí medzi jednotlivcov, ktorý urobia v danej oblasti hĺbkovú analýzu alebo návrh. Jednou z prvých a najdôležitejších úloh je vytvorenie databázového modelu, túto úlohu sme pridelili Georgimu Georgievovi, v čase stretnutia ešte nebol zverejnený CSV súbor, ktorý hrá dôležitú rolu pri návrhu databázy. Nemej dôležitý je aj grafický návrh stránky, ktorý má na starosti Daniel Melo, pri grafickom návrhu je dôležité dbať na to aby sa používateľ vedel ľahko orientovať na stránke. Ďalším krokom pri analýze a správnom návrhu softvéru je use-case diagram a dokumentácia. Vyhotoviť technickú dokumentáciu a zhotovenie use-case diagramu má Michal Varga. Súčasťou projektu budú nepochybne Google Maps, kde sa nám budú zobrazovať jednotlivé trasy športovcov. Možnosti implementácie Google máp do nášho projektu má na starosti Juraj Venczel. Na vyhotovenej stránke bude aj možnosť registrácie a po zaregistrovaní príde na zadaný email overovacia správa. Preskúmanie metód, ktoré sa v praxi využívajú a zvolenie vhodnej metódy do projektu má na starosti Matej Tkáč. Úlohou pre všetkých bolo vytvorenie konta na GitLabe a Daniel Melo vytvoril repozitár pre projekt.
Na ďalšom stretnutí sa náš tím bude rozdeľovať na menšie tímu, ktoré budú pracovať na nových úlohách.

					</p>
					<h1>Používateľské špecifikácia</h1>
					<h2>Úvod do problematiky:</h2>
					<p>
					Hlavnou úlohou projektu bude  vytvoriť stránku, kde sa bude dať sledovať na základe
odbehnutých/odjazdených kilometrov, ako sa približujeme k vytýčenému cieľu.
Potom na základe údajov, ktoré zadá užívateľ budeme postupne na vybranej trase zobrazovať „už prejdenú“ vzdialenosť. Počíta sa s tým, že užívateľ počas nejakého obdobia prejde postupne celú vytýčenú trasu.

					</p>
					<h2>Ciele projektu:</h2>
					<ul>
						<li>Registrácia</li>
						<li>Vytvorenie a zapamätanie trasy</li>
						<li> Aktivácia a deaktivácia trasy</li>
						<li>Definovanie módu trasy</li>
						<li>Možnosti prehliadania si trás</li>
						<li>Vytvorenie družstiev</li>
						<li>Zaznamenanie progresu</li>
						<li>Prehľad aktualít a newsletter</li>
					</ul>
					<h2>Doména:</h2>
					<p>
					Webová aplikácia bude naprogramovaná v jazykoch PHP, Python a JavaScript, s využitím SQL, HTML5 a CSS.
					</p>
					<h2>Use-case diagram:</h2>
					<img src="obrazkydodokumentacie/WT_Final_UC.png" style="width:880px;height:920px;"/>
					<h2>Použité knižnice a frameworky</h2>
					<p>
						<a href="http://pdfmake.org/">PDF Make</a>
						<a href="https://www.kryogenix.org/code/browser/sorttable/">Sortovanie tabuliek</a>
						<a href="https://colorlib.com/etc/cf/ContactFrom_v4/index.html">Bootstrap tempalte pre formulár(od toho sa odvíjala farebná schéma)</a>
						<a href="https://maps.googleapis.com/maps/api/js">Google maps API</a>
					</p>
					<h2>Defaultné heslá</h2>
					<p>0000</p>
					<h2>Mail adresa:</h2>
					<p>Adresa pre generovanie a odosielanie emailov pri registrácii a odbere noviniek: rand.acc.666@gmail.com</p>
					<h2>Hash hesla</h2>
					<p>hash('sha512', $password); -> return hash hesla</p>
					<h2>Generovanie validačného kódu pri overení registrácie užívateľa</h2>
					<p>Užívateľom email zadaný pri registráci bol zahashovaný funkciou: ripemd160. Po zahashovaní bol tento reťazec orezaný na prvých sedem znakov a tento kód bol následne odoslaný ako validačný kód daného užívateľa.</p>
					<h2>Inštalácie</h2>
					<p> sudo apt install ssmtp<br>
        sudo apt install msmtp ca-certificates<br>
        sudo apt install mailutils<br>
        sudo apt install postfix<br>
</p>


            
                

        </div>

    </div>

</div>


<!--===============================================================================================-->
<script src="../vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="../vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
<script src="../vendor/bootstrap/js/popper.js"></script>
<script src="../vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="../vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
<script src="../vendor/daterangepicker/moment.min.js"></script>
<script src="../vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
<script src="../vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
<script src="../js/main.js"></script>

</body>
</html>