<!DOCTYPE html>
<html lang="en">
<head>
    <title>WEBTE2 | Zadanie 3</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="../images/icons/favicon.ico"/>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../fonts/iconic/css/material-design-iconic-font.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/animsition/css/animsition.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/daterangepicker/daterangepicker.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../css/util.css">
    <link rel="stylesheet" type="text/css" href="../css/main.css">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <!--===============================================================================================-->
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <meta name="google-signin-scope" content="profile email">
    <meta name="google-signin-client_id" content="826090786652-7uu82dad1i2hs3olt99djahd65nk1ikg.apps.googleusercontent.com">
    <!--===============================================================================================-->
    <script src="../js/sorttable.js"></script>
    <script src='../js/pdfmake.min.js'></script>
    <script src='../js/vfs_fonts.js'></script>
    <!--===============================================================================================-->

<!-- ======================GOOGLE MAPS=================================== -->    
    <?php
    //DEBUG
	//	error_reporting(-1);
	//	ini_set('display_errors', 'On');
	//	set_error_handler("var_dump");
	?>


<!-- ===================END=OF=GOOGLE=MAPS=================================== -->

</head>
<body>
	<!-- ======================================NAVBAR============================================================== -->

	<?php
	   require ("navBar.php");
	   echo generateNavbarUser();
   ?>
<!-- ======================================NAVBAR============================================================== -->    
<style>
    .csv-btn:hover, #form_btn:hover{
        background-color: #d7ffff;
        transition: 1s;
    }

</style>
	<div class="limiter">
		<div class="container-login100" style="background-image: url('../images/bg-01.jpg');">
			<div class="wrap-login100 p-l-55 p-r-55 p-t-65 p-b-54 w-75" style="width: 70%; text-align: center; margin-top: 5%">
				<span class="login100-form-title p-b-49 ">
                    Finálne zadanie z predmetu WEBTE 2
				</span>

				<div class="txt1 text-center p-t-54 p-b-20">
					
					<div >
                		<br>
                		<?php
                		
                		session_start();
						require('../config.php');
						$connection = new mysqli($servername, $username, $password, $dbname);
						if ($connection->connect_error) 
						{
						    die("Connection failed: " . $connection->connect_error);
						}
						$connection->set_charset("utf8");
						

// ================================ activate/deactivate route =============================================================

                		if(isset($_POST['activate'])){

                			$routeID = $_POST['activate'];
                			
							$sqlRoute = "UPDATE `Route` SET `active`=1 WHERE id=$routeID;";// AND active=1;";
				            $resultRoute = $connection->query($sqlRoute);

				            $sqlRoute = "UPDATE `Route` SET `active`=0 WHERE id != $routeID;";// AND active=1;";
				            $resultRoute = $connection->query($sqlRoute);
							unset($startLats);
							unset($startLngs);
							unset($endLats);
							unset($endLngs);
							header("Refresh:0");
                		}

                		if(isset($_POST['deactivate'])){

                			$routeID = $_POST['deactivate'];
                			$sqlRoute = "UPDATE `Route` SET `active`=0 WHERE id=$routeID;";// AND active=1;";
				            $resultRoute = $connection->query($sqlRoute);

			            	unset($startLats);
							unset($startLngs);
							unset($endLats);
							unset($endLngs);
							header('Location: perfTable.php');


                		}


// ========================================================================================================================
            			if(isset($_SESSION['id']) )
            			{
            				// THIS DETERMINES USER ID
            				/*if usertype ADMIN
								set userID >> GET  //GEORGI
            				 */
							$userID = $_SESSION['id'];
							//CHANGE SRC OF userID to CHANGE DB OUTPUT

							//DB Setup
							
							//Route query
							$sqlRoute = "SELECT * FROM Route WHERE  user='".$userID."'";// AND active=1;";
				            $resultRoute = $connection->query($sqlRoute);

				            if(isset($_POST['hide']))
				            {
				            	unset($_POST); 
				            }

				            if(isset($_POST['routeID']))
				            {
				            	$routeID = $_POST['routeID'];
								$sqlRun = "SELECT * FROM Run WHERE route='".$routeID."';";
					            $resultRun = $connection->query($sqlRun);
					             
					        }

				            //Route results
				            if($resultRoute->num_rows > 0)
				            {
				            	/*
								
				            	*/
				            	$i = 1;
				            	//Route table HEADERS
				            	echo 'Here are your routes<br><br>';
					            echo '<table class="sortable table table-hover thead-dark" style="border:1px solid #dddddd">';
					            echo '<tr><th>id</th>
					            <th> START LAT </th>
					            <th> START LONG </th>
					            <th> END LAT </th>
					            <th> END LONG </th>
					            <th> ACTIVE </th>
					            <th> ACTIONS </th>
					            </tr>';

				                //Route results table
				                while ($rowRoute = $resultRoute->fetch_assoc()) 
				                {

				                	// print_r($rowRoute['active']);
				                	echo '<tr><td>'
					                	.$i.'</td><td>'
					                	.$rowRoute['start_lat'].'</td><td>'
					                	.$rowRoute['start_long']
					                	.'</td><td> '
					                	.$rowRoute['end_lat'].'</td><td> '
					                	.$rowRoute['end_long'].'</td><td>';
					                	if($rowRoute['active']==1){
					                		// echo '<span class="fa fa-check" style="color:green;"></span>';
					                		echo '<form action="perfTable.php" method="post" style="top:0;">
													<input type="hidden" name="deactivate" value="'.$rowRoute['id'].'">
													<button style="cursor:pointer;"  class="" type="submit" value=""><span class="fa fa-check" style="color:green;"></span></button>
												</form>';	
					                	}
					                	else {
					                		// echo '<span class="fa fa-close" style="color:red;"></span>';
					                		echo '<form action="perfTable.php" method="post" style="top:0;">
													<input type="hidden" name="activate" value="'.$rowRoute['id'].'">
													<button style="cursor:pointer;" class="" type="submit" value=""><span class="fa fa-close" style="color:red;"></span></button>
												</form>';	
					                	}
					                	echo '</td><td>'; 
									if(isset($_POST['routeID']) && $rowRoute['id']==$_POST['routeID'])
									{
										echo '<form action="perfTable.php" method="post" style="top:0;">
									<input type="hidden" name="hide" value="hide"><input style="hover: color:green;background-color:#ffffff;border-radius: 5px; border:1px solid #dddddd; color:#444444; cursor:pointer;" id="form_btn" class="login100-form-btn" type="submit" value="Hide details">
										</form></td></tr>';	
									}else{
										echo '<form action="perfTable.php" method="post" style="top:0;">
									<input type="hidden" name="routeID" value="'.$rowRoute['id'].'">
									<input style="hover: background-color:green; background-color:#ffffff; border-radius: 5px;border:1px solid #dddddd; color:#444444; cursor:pointer;" class="login100-form-btn" id="form_btn" type="submit" value="Show details">
									</form></td></tr>';
										}

									//Run results table
									if(isset($_POST['routeID']) &&  $rowRoute['id']==$_POST['routeID'])
									{
						            	$j = 1;
						            	echo '<tr><td colspan="7">';
										//Run table HEADERS
											echo '<table class="table sortable table-hover thead-light">';
											//echo '<div style="border-style:solid;border-color:red;border-width:5px">';
							                		echo '<tr><th>id </th>
							                		<th> DISTANCE </th>
							                		<th> START POINT </th>
							                		<th> END POINT </th>
							                		<th> DATE </th>
							                		<th> START TIME </th>
							                		<th> END TIME </th>
							                		<th> RATING </th>
							                		<th> NOTE </th>
							                		</tr>';

											while ($rowRun = $resultRun->fetch_assoc()) 
							                {
							                		echo '<tr><td>'
					                					.$j.'</td><td>'
					                					.$rowRun['distance'].' m'
					                					.'</td><td>'
					                					.$rowRun['start_lat'].', '
						                				.$rowRun['start_long']
						                				.'</td><td>'
					                					.$rowRun['end_lat'].', '
						                				.$rowRun['end_long']
						                				.'</td><td>'
						                				.$rowRun['date']
						                				.'</td><td>'
						                				.$rowRun['start_time']
						                				.'</td><td>'
						                				.$rowRun['end_time']
						                				.'</td><td>';

						                				if($rowRun['rating']==1){$color = '#ff5656';}
					                					if($rowRun['rating']==2){$color = '#ffd156';}
				                						if($rowRun['rating']==3){$color = '#e8ff56';}
			                							if($rowRun['rating']==4){$color = '#d8ff5b';}
		                								if($rowRun['rating']==5){$color = '#56ff64';}
						                				for($x=0; $x<$rowRun['rating']; $x++){
						                					echo '<span class="fa fa-thumbs-up" style="color: '.$color.';"></span> ';
						                				}
						                				// .$rowRun['rating']
						                				echo '</td><td>';
						                				echo $rowRun['note']
						                				.'</td></tr>';
							                			$j++;
											}
											mysqli_free_result($resultRun);
											echo '</table>';
										echo '</td></tr>'; 
									}
		                		$i++;
			                    }
							}
							mysqli_free_result($resultRoute);
							$connection->close();
							echo '</table>';
						}
						else{
							//redirect if session[id] is not set
							header('../index.php');
						}
						?>
                		<br>
                	</div>
					<span>

	    <?php 	
	//TODO: include php/googlesql.php
		require('googlesql.php');
		$currentUser = $_SESSION['id'];

		$startLats = returnCoordinates("start_lat", $currentUser);
		$startLngs = returnCoordinates("start_long", $currentUser);

		$endLats = returnCoordinates("end_lat", $currentUser);
		$endLngs = returnCoordinates("end_long", $currentUser);
		
		//print_r($startLats);
		//print_r($startLngs);
		
		echo '<hr/>';
		echo '<script>
				var startLats='.$startLats.';
				var startLngs='.$startLngs.';
				var endLats='.$endLats.';
				var endLngs='.$endLngs.';
			</script>';			

		

	?>
	<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js">
	    </script>
	    <script src="../js/googleMaps.js"></script>
	    	<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBJQv-yG16VjTsUX6w1By2gjN8a0R0a5Nc&callback=startRouteMap&libraries=places,geometry"
	  type="text/javascript">
	  	
	  </script>
                        <div class="flex-c-m">
	                    	<div id="map" style="float:center;	width:500px;	height:500px; border-style:solid;border-color:blue;border-width:3px"><!-- MAPA SEM  --></div>
	                   	</div>
					</span>
				</div>
        	</div>
		</div>
	</div>
	

<!--===============================================================================================-->
<script src="../vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="../vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
<script src="../vendor/bootstrap/js/popper.js"></script>
<script src="../vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="../vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
<script src="../vendor/daterangepicker/moment.min.js"></script>
<script src="../vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
<script src="../vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
<script src="../js/main.js"></script>

</body>
</html>