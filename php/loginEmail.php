<!DOCTYPE html>
<html lang="en">
<head>
    <title>WEBTE2 | Finálne zadanie</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="../images/icons/favicon.ico"/>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../fonts/iconic/css/material-design-iconic-font.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/animsition/css/animsition.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/daterangepicker/daterangepicker.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../css/util.css">
    <link rel="stylesheet" type="text/css" href="../css/main.css">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <!--===============================================================================================-->
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <meta name="google-signin-scope" content="profile email">
    <meta name="google-signin-client_id" content="826090786652-7uu82dad1i2hs3olt99djahd65nk1ikg.apps.googleusercontent.com">
    <!--===============================================================================================-->


 <!--===============================================================================================-->

</head>
     <!-- ======================================NAVBAR============================================================== -->
         
   <?php
   include "navBar.php";
   generateNavbarLogin();
?>
<!-- ======================================NAVBAR============================================================== -->
<body>
    <div class="limiter">
        <div class="container-login100" style="background-image: url('../images/bg-01.jpg');">

            <div class="wrap-login100 p-l-55 p-r-55 p-t-65 p-b-54 w-50" style="width: 70%; text-align: center; margin-top: 5%">
                    <span class="login100-form-title p-b-49">
                        Registration
                    </span>
                    <br>
                    <br>
                <?php 
                    if(isset($_POST['submit'])){
                        session_start();

                        $mailServer = "rand.acc.666@gmail.com";     // this is your Email address
                        $userEmail = $_POST['email'];               // this is the sender's Email address

                        //Data from form
                        $first_name = $_POST['first_name'];
                        $last_name = $_POST['last_name'];
                        $school = $_POST['school'];
                        $password = $_POST['password'];
                        $address = $_POST['address'];

                        //Insert to DB
                        require('../config.php');

                        $query = "INSERT INTO User"
                        ."(first_name,last_name, school, address, password, email, type)"
                        ." VALUES "
                        ."('".$first_name."','".$last_name."','".$school."','".$address."','".hash('sha512', $password)."','".$userEmail."', 2);";
                        
                        $connection = mysqli_connect($servername,$username,$password,$dbname);
                        $connection->set_charset("UTF8");
                        $result = mysqli_query($connection, $query);

                        //If query passed
                        if($result){
                            $loginLink="http://147.175.98.241/final-zadanie/master/php/adminLogin.php";
                            //todo: code gen
                                $hash_mail = hash('ripemd160', $userEmail);
                                // echo $hash_mail;
                                $cut_hash = substr($hash_mail, 0, 7);
                            $authCode = $cut_hash;       

                            //Confirmation e-mail
                            $headers = "From:" . $mailServer;
                            $subject = "Registration confirmation e-mail";
                            $message = "This is an confirmation e-mail.\n\n\n Your authentication code: ".$authCode." \n\n\nYou can follow this link:\n\n\n http://147.175.98.241/final-zadanie/team2/php/adminLogin.php" ;
                            mail($userEmail,$subject,$message,$headers);
                            
                            //Copy of confirmation e-mail for us
                            $subject2 = "Copy of confirmation e-mail";
                            $message2 = "Following was just sent to ".$_POST['email'].":\n\n\n\"".$message."\n\".";
                            $headers2 = "From:" . $userEmail;
                            mail($mailServer,$subject2,$message2,$headers2);

                            echo "<font color=\"green\">Confirmation e-mail sent to address ".$to."</font>";

                        }
                        else
                            echo "<font color=\"red\">User with that email alredy exists! </font>";
                            echo '<br><br><div class="txt1 text-center p-t-54 p-b-20" style="margin-top: 0;padding-top: 10px">
                                    <span>
                                        <div class="">
                                    <a href="loginEmail.php" class=""> 
                                        <!-- TODO doplnit link -->
                                        Try again
                                    </a>
                                    </span>
                                </div>';
                        //Close everything
                        mysqli_free_result($result);
                        $connection->close();
                        session_destroy();
                    }else{

                    echo '<form class="login100-form validate-form" action="loginEmail.php" method="post" >';
                        echo '<div class="wrap-input100 validate-input m-b-23" data-validate = "First Name is required">
                            <span class="label-input100">First name</span>
                            <input class="input100" type="text" name="first_name" placeholder="Type your first name">
                            <span class="focus-input100" data-symbol="&#xf206;"></span>
                            </div>';
                            echo '<br>';

                        echo '<div class="wrap-input100 validate-input m-b-23" data-validate = "Last Name is required">
                            <span class="label-input100">Last name</span>
                            <input class="input100" type="text" name="last_name" placeholder="Type your last name">
                            <span class="focus-input100" data-symbol="&#xf206;"></span>
                            </div>';
                            echo '<br>';

                        echo '<span class="label-input100">School</span>';
                        echo '<select name="school" id="school" class=" form-control">';
                                require('../config.php'); 
                                $query = "SELECT * FROM School WHERE 1";
                                $connection = mysqli_connect($servername,$username,$password,$dbname);
                                $connection->set_charset("UTF8");
                                $result = mysqli_query($connection, $query);
                                while( $row = $result->fetch_assoc() )
                                    echo '<option value="'.$row['id'].'">'.$row['name'] .' - '. $row['address'].'</option>';
                                mysqli_free_result($result);
                                $connection->close();
                        echo '</select><br>';
                        
                        echo '<div class="wrap-input100 validate-input m-b-23" data-validate = "School is required">
                            <span class="label-input100">Password</span>
                            <input class="input100" type="Password" name="password" placeholder="Type your password">
                            <span class="focus-input100" data-symbol="&#xf206;"></span>
                            </div>';
                            echo '<br>';

                    echo '<div class="wrap-input100 validate-input m-b-23" data-validate = "Username is reauired">
                        <span class="label-input100">Adress</span>
                        <input class="input100" type="text" name="address" placeholder="Type your adress">
                        <span class="focus-input100" data-symbol="&#xf206;"></span>
                        </div>';
                        echo '<br>';

                    echo '<div class="wrap-input100 validate-input m-b-23" data-validate = "Username is reauired">
                        <span class="label-input100">Email</span>
                        <input class="input100" type="text" name="email" placeholder="Type your email adress">
                        <span class="focus-input100" data-symbol="&#xf206;"></span>
                        </div>';
                        echo '<br>';

                        echo '<div class="container-login100-form-btn">
                        <div class="wrap-login100-form-btn">
                        <div class="login100-form-bgbtn"></div>
                        <button type="submit" class="login100-form-btn" name="submit">
                            Submit
                        </button>
                        </div>
                        </div>';
                        echo '<br>';
                    echo '</form>';
                    }
                ?>
                <div class="txt1 text-center p-t-54 p-b-20" style="margin-top: 0;padding-top: 0">
                        <span>
                            or
                            <div class="">
                        <a href="adminLogin.php" class="" style="width: 50px; height: 50px"> 
                            Login
                        </a>
                        </span>
                </div>
        </div>
        </div>
    </div>
    <?php
        print_r($_POST);
    ?>
</body>
</html>