<!DOCTYPE html>
<html lang="en">
<head>
    <title>WEBTE2 | Zadanie 3</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="../images/icons/favicon.ico"/>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../fonts/iconic/css/material-design-iconic-font.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/animsition/css/animsition.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/daterangepicker/daterangepicker.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../css/util.css">
    <link rel="stylesheet" type="text/css" href="../css/main.css">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <!--===============================================================================================-->
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <meta name="google-signin-scope" content="profile email">
    <meta name="google-signin-client_id" content="826090786652-7uu82dad1i2hs3olt99djahd65nk1ikg.apps.googleusercontent.com">
    <!--===============================================================================================-->
    <script src="../js/sorttable.js"></script>
    <script src='../js/pdfmake.min.js'></script>
    <script src='../js/vfs_fonts.js'></script>
    <!--===============================================================================================-->


</head>
<body onload="fill_selects()">
    
<!-- ======================================NAVBAR============================================================== -->
         
   <?php
   session_start();
   if(!isset($_SESSION) || $_SESSION['type']!=4){
        header('Location: noPermission.php');

   }
   require ("navBar.php");
   echo generateNavbarAdmin();
   ?>
<!-- ======================================NAVBAR============================================================== -->
<?php 
    require '../config.php';
    $mysqli = new mysqli($servername,$username,$password,$dbname);
    $mysqli->set_charset("UTF8");


    if ($mysqli->connect_error) {
        die("Chyba :(");
    }
   
    $userArray = [];
    $tmp = [];
    $query = "SELECT * FROM `User`";

    $result = mysqli_query($mysqli, $query);
    while( $row = $result->fetch_assoc() )
    {
        array_push($tmp,$row['id'],$row['first_name'].' '.$row['last_name'] .' - '. $row['address']);
        array_push($userArray,$tmp);
        $tmp = [];
    }
       
    mysqli_free_result($result);
    $mysqli->close();

    echo 
    
    "
    <script>
    var users =".json_encode($userArray)."
    </script>
    ";

                ?>
    </div>        
<div class="limiter">
    <div class="container-login100" style="background-image: url('../images/bg-01.jpg');">
        <div class="wrap-login100 p-l-55 p-r-55 p-t-65 p-b-54 w-50" style="margin-top: 4%">

            <span class="login100-form-title p-b-49">
                Group Management
            </span>

                <!-- <div class="container-login100-form-btn w-25"> -->
                    <form class="login100-form validate-form" action="groupManagementAdmin.php" name="selected_people" id="poeple_selects" method="post">
                    <span class="label-input100">Choose people you want to add to new group</span>
                        
                    </span>

                <div class="wrap-input100 validate-input m-b-23" data-validate = "Group name is required">
                    <span class="label-input100">Group name</span>
                    <input class="input100" type="text" name="group_name" placeholder="Type name of the group you want to create">
                    <span class="focus-input100" data-symbol="&#xf206;"></span>
                </div>
                <div class="wrap-input100 m-b-23" >
                    <span class="label-input100"></span>
                    <select onchange="change_array(1)" name="people_sel1" id="people_sel1" class=" form-control">
                        <option value="0">Choose human1</option>
                    </select><br>
                    <!-- <span class="focus-input100" data-symbol="&#xf206;"></span> -->
                </div>
                <div class="wrap-input100 m-b-23" >
                    <span class="label-input100"></span>
                    <select onchange="change_array(2)" name="people_sel2" id="people_sel2" class=" form-control">
                        <option value="0">Choose human2</option>

                    </select><br>
                    <!-- <span class="focus-input100" data-symbol="&#xf206;"></span> -->
                </div>
                <div class="wrap-input100 m-b-23" >
                    <span class="label-input100"></span>
                    <select onchange="change_array(3)" name="people_sel3" id="people_sel3" class=" form-control">
                        <option value="0">Choose human3</option>

                    </select><br>
                    <!-- <span class="focus-input100" data-symbol="&#xf206;"></span> -->
                </div>
               <div class="wrap-input100 m-b-23" >
                    <span class="label-input100"></span>
                    <select onchange="change_array(4)" name="people_sel4" id="people_sel4" class=" form-control">
                        <option value="0">Choose human4</option>

                    </select><br>
                    <!-- <span class="focus-input100" data-symbol="&#xf206;"></span> -->
                </div>
                <div class="wrap-input100 m-b-23" >
                    <span class="label-input100"></span>
                    <select onchange="change_array(5)" name="people_sel5" id="people_sel5" class=" form-control">
                        <option value="0">Choose human5</option>

                    </select><br>
                    <!-- <span class="focus-input100" data-symbol="&#xf206;"></span> -->
                </div>
                <div class="wrap-input100 m-b-23" >
                    <span class="label-input100"></span>
                    <select onchange="change_array(6)" name="people_sel6" id="people_sel6" class=" form-control">
                        <option value="0">Choose human6</option>

                    </select><br>
                    <!-- <span class="focus-input100" data-symbol="&#xf206;"></span> -->
                </div>
                <br>

                <div class="container-login100-form-btn">
                    <div class="wrap-login100-form-btn">
                        <div class="login100-form-bgbtn"></div>
                        <button type="button" onclick="submitForm()" class="login100-form-btn" name="selected_people">
                            Create group
                        </button>
                    </div>
                </div>

            </form>
                    
        </div>

    </div>

</div>

<script type="text/javascript">
    // var people = ["clovek1","clovek2", "clovek3", "clovel4", "clovek5"];
    // var selected_people_array= [];
    function fill_selects(){
        console.log(users[0][1]);
        for(var j=1; j<=6; j++){
        var sel = document.getElementById('people_sel'+j+'');
        for(var i = 0; i < users.length; i++) {
            var opt = document.createElement('option');
            opt.innerHTML = users[i][1];
            opt.value = users[i][0];
            sel.appendChild(opt);
        }
        }     
    }
    function change_array(select_num){
        console.log(select_num);
        var select = document.getElementById('people_sel'+select_num+'');
        var select_val = select.value;
        // console.log(select_val);
        for(var i=1; i<=6; i++){
            // console.log(i);
            if(i == select_num){
                continue;
            }
            var select_loop = document.getElementById('people_sel'+i+'');
            var  value_loop = select_loop.value;
            // console.log(select_val+'----'+value_loop);

            if(value_loop == select_val && value_loop!=0 && select_val!=0){
                alert('Please choose another user, this is already selected');
            }
        }
    }

    function submitForm(){
        var control_var = 1;
        for(var i=1; i<=6; i++){
            for(var j=1; j<=6; j++){
                var select_i = document.getElementById('people_sel'+i+'');
                var select_val_i = select_i.value;    
                var select_j = document.getElementById('people_sel'+j+'');
                var select_val_j = select_j.value;
                // console.log(select_val_i+'*'+select_val_j);
                if(i==j){
                    continue;
                }
                if(select_val_i == select_val_j && select_val_i!=0 && select_val_j!=0) {
                    control_var = 0;
                    break;     
                } 
            }
        }
        if(control_var == 0){
            alert('You choosed some people multiple times, change it to make it work!');
        }
        else if(control_var == 1){
            // console.log('no u');
            var ctrl_var = 0;
            for(var k=1; k<=6; k++){
                var select_k = document.getElementById('people_sel'+k+'');
                var select_val_k = select_k.value;   
                if(select_val_k!=0){
                    ctrl_var++;
                    // selected_people_array.push(select_val_k);

                }
            }
            if(ctrl_var!=0){
                // console.log(selected_people_array);
            document.getElementById("poeple_selects").submit(); 
            }
            else {
                alert("You must select at least one person!");
            }
        }
        
        }
    
    </script>
     <?php

    if(isset($_POST['people_sel1'])){
        require '../config.php';
        $mysqli = new mysqli($servername,$username,$password,$dbname);
        $mysqli->set_charset("UTF8");


        if ($mysqli->connect_error) {
            die("Chyba :(");
        }

    
        $sql = "INSERT INTO `Group`(`name`) VALUES ('".$_POST['group_name']."')";
        $result = mysqli_query($mysqli, $sql);

        $sql = "SELECT `id` FROM `Group` WHERE name = '".$_POST['group_name']."'";
        $result = mysqli_query($mysqli, $sql);
     
        $row = $result->fetch_assoc();
        $id =  $row['id'];
        
        // SELECT `id` FROM `Group` WHERE name = 'bob'3Array ( [group_name] => bob [people_sel1] => 1 [people_sel2] => 0 [people_sel3] => 0 [people_sel4] => 0 [people_sel5] => 0 [people_sel6] => 0 ) 
        if ($_POST['people_sel1'] != 0)
        {
            $sql = "INSERT INTO `Group_user`(`user`, `group`) VALUES (".$_POST['people_sel1'].",".$id.")";
            $result = mysqli_query($mysqli, $sql);
        }
        if ($_POST['people_sel2'] != 0)
        {
            $sql = "INSERT INTO `Group_user`(`user`, `group`) VALUES (".$_POST['people_sel2'].",".$id.")";
            $result = mysqli_query($mysqli, $sql); 
        }
        if ($_POST['people_sel3'] != 0)
        {
            $sql = "INSERT INTO `Group_user`(`user`, `group`) VALUES (".$_POST['people_sel3'].",".$id.")";
            $result = mysqli_query($mysqli, $sql);
        }
        if ($_POST['people_sel4'] != 0)
        {
            $sql = "INSERT INTO `Group_user`(`user`, `group`) VALUES (".$_POST['people_sel4'].",".$id.")";
            $result = mysqli_query($mysqli, $sql);
        }
        if ($_POST['people_sel5'] != 0)
        {
            $sql = "INSERT INTO `Group_user`(`user`, `group`) VALUES (".$_POST['people_sel5'].",".$id.")";
            $result = mysqli_query($mysqli, $sql);
        }
        if ($_POST['people_sel6'] != 0)
        {
            $sql = "INSERT INTO `Group_user`(`user`, `group`) VALUES (".$_POST['people_sel6'].",".$id.")";
            $result = mysqli_query($mysqli, $sql);
        }

        mysqli_free_result($result);
        $mysqli->close();
    }
?>
<!--===============================================================================================-->
<script src="../vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="../vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
<script src="../vendor/bootstrap/js/popper.js"></script>
<script src="../vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="../vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
<script src="../vendor/daterangepicker/1moment.min.js"></script>
<script src="../vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
<script src="../vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
<script src="../js/main.js"></script>

</body>
</html>