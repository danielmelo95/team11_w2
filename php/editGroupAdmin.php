<!DOCTYPE html>
<html lang="en">
<head>
    <title>WEBTE2 | Zadanie 3</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="../images/icons/favicon.ico"/>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../fonts/iconic/css/material-design-iconic-font.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/animsition/css/animsition.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/daterangepicker/daterangepicker.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../css/util.css">
    <link rel="stylesheet" type="text/css" href="../css/main.css">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <!--===============================================================================================-->
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <meta name="google-signin-scope" content="profile email">
    <meta name="google-signin-client_id" content="826090786652-7uu82dad1i2hs3olt99djahd65nk1ikg.apps.googleusercontent.com">
    <!--===============================================================================================-->
    <script src="../js/sorttable.js"></script>
    <script src='../js/pdfmake.min.js'></script>
    <script src='../js/vfs_fonts.js'></script>
    <!--===============================================================================================-->


</head>
<body onload="fill_selects()">
<!-- ======================================NAVBAR============================================================== -->

<?php
    session_start();
   if(!isset($_SESSION) || $_SESSION['type']!=4){
        header('Location: noPermission.php');

   }
    require ("navBar.php");
    echo generateNavbarAdmin();
   ?>
<!-- ======================================NAVBAR============================================================== --> 
<?php

function deleteUser($groupId,$userId) {
    require '../config.php';
    $mysqli = new mysqli($hostname,$username,$password,$dbname);
    $mysqli->set_charset("cp1250");
    if (mysqli_connect_errno()) {
        echo "Failed to connect to MySQL: " . mysqli_connect_error();
    }
    else
    {
       
        $sql = "DELETE FROM `Group_user`
        WHERE user = ".$userId." 
        AND `group` = ".$groupId;
        $result = $mysqli->query($sql);
    }
    mysqli_close($mysqli);

}
function addUser($groupId,$userId) {
    require '../config.php';
    $mysqli = new mysqli($hostname,$username,$password,$dbname);
    $mysqli->set_charset("cp1250");
    if (mysqli_connect_errno()) {
        echo "Failed to connect to MySQL: " . mysqli_connect_error();
    }
    else
    {
        $sql = "SELECT * FROM `Group_user` " ." WHERE user = ".$userId." 
        AND `group` = ".$groupId; 
        $result = $mysqli->query($sql);
        if ($result->num_rows != 0) {
            header("Location: editGroupAdmin.php");
            
        }
        else
        {
        $sql = 
            "INSERT INTO `Group_user`(`user`, `group`) 
             VALUES (".$userId.",".$groupId.")";
        $result = $mysqli->query($sql);
        }
    }
    mysqli_close($mysqli);
}

require '../config.php';

    $userArray = [];
    $tmp = [];
    $mysqli = new mysqli($hostname,$username,$password,$dbname);
     $mysqli->set_charset("UTF8");
    $query = "SELECT * FROM `User`";

    $result = mysqli_query($mysqli, $query);
    while( $row = $result->fetch_assoc() )
    {
        array_push($tmp,$row['id'],$row['first_name'].' '.$row['last_name'] .' - '. $row['address']);
        array_push($userArray,$tmp);
        $tmp = [];
    }
    mysqli_free_result($result);
    $mysqli->close();
    
    echo 
    "
    <script>
    var users =".json_encode($userArray).";
    console.log(users);
    </script>
    ";
?>
<style>
    .csv-btn:hover{
        background-color: #d7ffff;
        transition: 1s;
    }
</style>


<div class="limiter">
    <div class="container-login100" style="background-image: url('../images/bg-01.jpg');">
        <div class="wrap-login100 p-l-55 p-r-55 p-t-65 p-b-54 w-75" style="margin-top: 4%">
        <div class="wrap-input100 m-b-23" >

            <form class="login100-form validate-form" action="editGroupAdmin.php" name="selected_people" id="poeple_selects" method="post">
                    <span class="label-input100">Choose people you want to add to new group</span>
                        
            <span class="label-input100"></span>
            <select onchange="change_array(1)" name="people_sel1" id="people_sel1" class=" form-control">
                <option value="0">Choose human1</option>
            </select><br>
                    <!-- <span class="focus-input100" data-symbol="&#xf206;"></span> -->
                                    <div class="container-login100-form-btn">
                    <div class="wrap-login100-form-btn">
                        <div class="login100-form-bgbtn"></div>
                        <button type="button" onclick="submitForm()" class="login100-form-btn" name="selected_people">
                            Add user to group
                        </button>
                    </div>
                </div>
        </div>
    </form>
<?php
session_start();

if(isset($_GET['id'])){
    $_SESSION['gId'] = $_GET['id'];
}

if(isset($_POST['delete'])){
    // echo $_POST['delete'].'---><br>';
    $_SESSION['gId'];

    deleteUser($_SESSION['gId'],$_POST['delete']);
    // echo $_SESSION['gId'];
    header("Location: editGroupAdmin.php");
}

if(isset($_POST['people_sel1'])){
    // echo $_POST['delete'].'---><br>';
    $_SESSION['gId'];
    
    require '../config.php';

    $mysqli = new mysqli($hostname,$username,$password,$dbname);
    $mysqli->set_charset("cp1250");

    $sql = "SELECT COUNT(DISTINCT user) as cnt FROM Group_user WHERE `group` = ".$_SESSION['gId'];
    $result = $mysqli->query($sql);  
    $res = $result->fetch_assoc();
    if($res['cnt']<6){
        addUser($_SESSION['gId'],$_POST['people_sel1']);

    }

    header("Location: editGroupAdmin.php");
}

// if (mysqli_connect_errno()) {
//     echo "Failed to connect to MySQL: " . mysqli_connect_error();
// }
// else
// {
//         // print_r($_POST);
    require '../config.php';

    $mysqli = new mysqli($hostname,$username,$password,$dbname);
    $mysqli->set_charset("cp1250");

    $sql = "SELECT ss.*, CONCAT(first_name,' ',last_name,' ', address) as user 
    FROM User u JOIN (SELECT g.id gID, g.name gName, gu.user uId 
    FROM `Group` g JOIN Group_user gu ON g.id = gu.group)ss ON ss.uID = u.id WHERE gID =". $_SESSION['gId'];
    $result = $mysqli->query($sql);  
    echo "<div>";
    if ($result->num_rows > 0) 
    {
        echo "<table class = 'sortable table'>";
        
        echo "
        <tr>
        <th>Group Id </th>

        <th>Group Name</th>

        <th>User ID</th>

        <th>User</th>
        
        <th></th>
        </tr>
        ";

        while ($row = $result->fetch_object())
        {
            echo "<tr class=\" value\">";
            $user_id ;
            foreach ($row as $value)
            {
                echo "<td>". iconv( "Windows-1250", "UTF-8", ($value)) . "</td>";
                $user_id = $value->uId;
            }


            echo '<td>
                <form action="editGroupAdmin.php" method="post" style="top:0;">
                    <input type="hidden" name="delete" value="'.$row->uId.'">
                    <button style="cursor:pointer;" type="submit" value="">
                        <span class="fa fa-trash" style="color: red">
                        </span>
                    </button>
                </form>
                </td></tr>';

        }

        echo "</table>";
    }
    echo "</div>";
// }
mysqli_close($mysqli);

?>



    </div>
</div>
</div>

<script type="text/javascript">
    // var people = ["clovek1","clovek2", "clovek3", "clovel4", "clovek5"];
    // var selected_people_array= [];
    function fill_selects(){
        console.log(users);
        
        var sel = document.getElementById('people_sel1');
        for(var i = 0; i < users.length; i++) {
            var opt = document.createElement('option');
            opt.innerHTML = users[i][1];
            opt.value = users[i][0];
            sel.appendChild(opt);
        }   
    }

    function submitForm(){
        var control_var = 0;
        
                var select_k = document.getElementById('people_sel1');
                var select_val_k = select_k.value;   
                if(select_val_k!=0){
                    ctrl_var=1;
                    // selected_people_array.push(select_val_k);

            if(ctrl_var!=0){
                // console.log(selected_people_array);
            document.getElementById("poeple_selects").submit(); 
            }
            else {
                alert("You must select at least one person!");
            }
        }
    }

        
        
    </script>

<!--===============================================================================================-->
<script src="../vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="../vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
<script src="../vendor/bootstrap/js/popper.js"></script>
<script src="../vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="../vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
<script src="../vendor/daterangepicker/moment.min.js"></script>
<script src="../vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
<script src="../vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
<script src="../js/main.js"></script>

</body>
</html>