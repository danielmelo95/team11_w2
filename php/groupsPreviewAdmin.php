<!DOCTYPE html>
<html lang="en">
<head>
    <title>WEBTE2 | Zadanie 3</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="../images/icons/favicon.ico"/>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../fonts/iconic/css/material-design-iconic-font.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/animsition/css/animsition.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/daterangepicker/daterangepicker.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../css/util.css">
    <link rel="stylesheet" type="text/css" href="../css/main.css">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <!--===============================================================================================-->
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <meta name="google-signin-scope" content="profile email">
    <meta name="google-signin-client_id" content="826090786652-7uu82dad1i2hs3olt99djahd65nk1ikg.apps.googleusercontent.com">
    <!--===============================================================================================-->
    <script src="../js/sorttable.js"></script>
    <script src='../js/pdfmake.min.js'></script>
    <script src='../js/vfs_fonts.js'></script>
    <!--===============================================================================================-->


</head>
<body>
<!-- ======================================NAVBAR============================================================== -->

<?php
    session_start();
   if(!isset($_SESSION) || $_SESSION['type']!=4){
        header('Location: noPermission.php');

   }
    require ("navBar.php");
    echo generateNavbarAdmin();
   ?>
<!-- ======================================NAVBAR============================================================== --> 

<style>
    .csv-btn:hover{
        background-color: #d7ffff;
        transition: 1s;
    }
</style>


<div class="limiter">
    <div class="container-login100" style="background-image: url('../images/bg-01.jpg');">
        <div class="wrap-login100 p-l-55 p-r-55 p-t-65 p-b-54 w-50" style="margin-top: 4%">

<?php

require '../config.php';

$mysqli = new mysqli($hostname,$username,$password,$dbname);
$mysqli->set_charset("cp1250");
if (mysqli_connect_errno()) {
    echo "Failed to connect to MySQL: " . mysqli_connect_error();
}
else
{
    $sql = "SELECT g.id , g.name , COUNT(gu.user) as cnt FROM `Group` g JOIN Group_user gu ON g.id = gu.group GROUP BY g.id ";
    $result = $mysqli->query($sql);
    echo "<div>";
    if ($result->num_rows > 0) 
    {
        echo "<table class = 'sortable table'>";
        
        echo "
        <tr>
        <th>Id </th>

        <th>Name</th>

        <th>Runners Count</th>

        <th>View</th>

        </tr>
        ";
        while ($row = $result->fetch_object())
        {
            echo "<tr class=\" value\">";

            foreach ($row as $value)
            {
                echo "<td>". $value . "</td>";
            }
            echo "<th><a class = 'fa fa-arrow-right' href='editGroupAdmin.php?id=".$row->id."'></a></th>";
            echo "</tr>";

        }

        echo "</table>";
    }
    echo "</div>";
}
   

?>



    </div>
</div>
</div>


<!--===============================================================================================-->
<script src="../vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="../vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
<script src="../vendor/bootstrap/js/popper.js"></script>
<script src="../vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="../vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
<script src="../vendor/daterangepicker/moment.min.js"></script>
<script src="../vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
<script src="../vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
<script src="../js/main.js"></script>

</body>
</html>